<?php defined('BASEPATH') OR exit('No direct script access allowed');
	function error_404(){
		$CI =& get_instance();
		$data = array('content' => '404');
		$CI->load->view('template', $data);
	}
	
	function e_debug($data = null, $dump = false){
		echo '<pre>';
		if ($dump) {
			var_dump($data);
		} else {		
			print_r($data);
		}
		echo '</pre>';
	}
	
	function x_debug($data = null, $dump = false){
		e_debug($data, $dump); die;
	}
	
	function getLoadedFiles(){
		echo '<pre>';
		print_r(get_included_files());
		echo '</pre>';
	}
	
	function start_time(){ 
      $mtime = microtime(); 
      $mtime = explode(' ', $mtime); 
      return $mtime;
   }
  
   function finish_time($starttime){
      $starttime = $starttime[1] + $starttime[0];
      $mtime = microtime();
      $mtime = explode(" ", $mtime);
      $mtime = $mtime[1] + $mtime[0];
      return ($mtime - $starttime);
   }
	
	function getServerDate($day = true, $time = false){
	    $hari  = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
	    $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
		$date = '';		
		if($day){
			$date.= $hari[date('w')].', ';
		}
		$date .= date('j').' '.$bulan[date('n')-1].' '.date('Y');
		if($time){
			$date .= "&nbsp;&nbsp;&nbsp;".date("G:i:s");
		}
		return $date;
	}
	
	function trim_string($string){
		return preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
	}

	function idn_date ($timestamp = '', $date_format = 'l, j F Y', $suffix = '') {
		if (trim ($timestamp) == '')
		{
				$timestamp = time ();
		}
		elseif (!ctype_digit ($timestamp))
		{
			$timestamp = strtotime ($timestamp);
		}
		# remove S (st,nd,rd,th) there are no such things in indonesia :p
		$date_format = preg_replace ("/S/", "", $date_format);
		$pattern = array (
			'/Mon[^day]/','/Tue[^sday]/','/Wed[^nesday]/','/Thu[^rsday]/',
			'/Fri[^day]/','/Sat[^urday]/','/Sun[^day]/','/Monday/','/Tuesday/',
			'/Wednesday/','/Thursday/','/Friday/','/Saturday/','/Sunday/',
			'/Jan[^uary]/','/Feb[^ruary]/','/Mar[^ch]/','/Apr[^il]/','/May/',
			'/Jun[^e]/','/Jul[^y]/','/Aug[^ust]/','/Sep[^tember]/','/Oct[^ober]/',
			'/Nov[^ember]/','/Dec[^ember]/','/January/','/February/','/March/',
			'/April/','/June/','/July/','/August/','/September/','/October/',
			'/November/','/December/',
		);
		$replace = array ( 'Sen','Sel','Rab','Kam','Jum','Sab','Min',
			'Senin','Selasa','Rabu','Kamis','Jumat','Sabtu','Minggu',
			'Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nov','Des',
			'Januari','Februari','Maret','April','Juni','Juli','Agustus','Sepember',
			'Oktober','November','Desember',
		);
		$date = date ($date_format, $timestamp);
		$date = preg_replace ($pattern, $replace, $date);
		$date = "{$date} {$suffix}";
		return $date;
	} 
	
    function getDropdownMonth($name = 'dd', $currentMonth = 1, $full = TRUE, $opt = null){
        $mon = ($full)? array(1 => 'Januari', 2 => 'Februari', 3 => 'Maret', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli', 8 => 'Agustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'Desember') :
                        array(1 => 'Jan', 2 => 'Feb', 3 => 'Mar', 4 => 'Apr', 5 => 'Mei', 6 => 'Jun', 7 => 'Jul', 8 => 'Aug', 9 => 'Sep', 10 => 'Okt', 11 => 'Nov', 12 => 'Des');

        $dd = '<select id="'.$name.'" name="'.$name.'" '.strip_tags($opt).'>';
        foreach($mon as $i => $m){
            $dd .= '<option value="'.$i.'" '.(($i == $currentMonth)? 'selected' : '').'>'.$m.'</option>';
        }
        $dd .= '</select>';
        return $dd;
    }

    function getDropdownYear($name = 'dd', $range = [], $currentYear = 1, $opt = null){
        $dd = '<select id="'.$name.'" name="'.$name.'" '.strip_tags($opt).'>';
        foreach($range as $m){
            $dd .= '<option value="'.$m.'" '.(($m == $currentYear)? 'selected' : '').'>'.$m.'</option>';
        }
        $dd .= '</select>';
        return $dd;
    }
	
	function getMonthName($no = 1, $full = TRUE){
		$no = (int) $no;
		
		$month = array(	1 => 'Januari',
						2 => 'Februari',
						3 => 'Maret',
						4 => 'April',
						5 => 'Mei',
						6 => 'Juni',
						7 => 'Juli',
						8 => 'Agustus',
						9 => 'September',
						10 => 'Oktober',
						11 => 'November',
						12 => 'Desember');
		$mon   = array(	1 => 'Jan',
						2 => 'Feb',
						3 => 'Mar',
						4 => 'Apr',
						5 => 'May',
						6 => 'Jun',
						7 => 'Jul',
						8 => 'Aug',
						9 => 'Sep',
						10 => 'Oct',
						11 => 'Nov',
						12 => 'Dec');
						
		if($full){
			return (array_key_exists($no, $month))? $month[$no] : ' ';
		}else{
			return (array_key_exists($no, $mon))? $mon[$no] : ' ';
		}
	}
	
	function getDayName($date){
		$day = date('N', strtotime($date));
		
		switch($day){
			case 1 : $day = 'Senin'; break;
			case 2 : $day = 'Selasa'; break;
			case 3 : $day = 'Rabu'; break;
			case 4 : $day = 'Kamis'; break;
			case 5 : $day = 'Jumat'; break;
			case 6 : $day = 'Sabtu'; break;
			case 7 : $day = 'Minggu'; break;
		}
		return $day;
	}
	
	
	function modal_message($tipe = 'danger', $title = '', $message = ''){
		$CI =& get_instance();
		$data = array(	'tipe' => $tipe,
						'title' => $title,
						'message' => $message);
		$CI->load->view('component/modal_message', $data);
	}
	
	function guid(){
		mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = chr(123)// "{"
				.substr($charid, 0, 8).$hyphen
				.substr($charid, 8, 4).$hyphen
				.substr($charid,12, 4).$hyphen
				.substr($charid,16, 4).$hyphen
				.substr($charid,20,12)
				.chr(125);// "}"
			return $uuid;
	}
	
	function netral_currency($money){
		return (float) str_replace('.', '', $money);
	}
	
	function netral_number($number){
		return (int) str_replace('.', '', $number);
	}
	
	function convertDate($date, $format = 'd M Y'){
		$d = new DateTime($date);
		return $d->format($format);
	}
	
	function validateDate($date, $format = 'Y-m-d H:i:s'){
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    function convertValidDate($date, $source_format = 'd-m-Y', $destination_format = 'Y-m-d'){
        if(validateDate($date, $source_format)){
            //$date = convertDate($date, $destination_format);
            $date = DateTime::createFromFormat($source_format, $date);
            $date = $date->format($destination_format);
        }else{
            $date = date($destination_format);
        }
        return $date;
    }

    function generateRandomString($length = 10, $cs = true) {
        $characters = ($cs)? '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ' : '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function formatIDR($money){
        $fmt = new NumberFormatter( 'id_ID', NumberFormatter::CURRENCY );
        $fmt->setSymbol(NumberFormatter::CURRENCY_SYMBOL, '');
        return $fmt->formatCurrency($money, "IDR");
    }

    function formatMoney($money){
        return number_format($money, 2, ',', '.');
    }