<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="import-form" width="80%">
<div id="modal-import" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-4 col-form-label">Kategori KTP</label>
				<div class="col-sm-8">
					<select id="int_kategori_ktp_id" name="int_kategori_ktp_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih Kategori-</option>
						<?php 
							foreach($kategori as $k){
								echo '<option value="'.$k->int_kategori_ktp_id.'">'.$k->var_kategori_ktp.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="customFile" class="col-sm-4 col-form-label">File Import</label>
				<div class="col-sm-8">
					<div class="custom-file">
						<input type="file" class="custom-file-input" name="<?php echo $input_file_name?>" id="customFile" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
						<label class="custom-file-label" for="customFile">Pilih File</label>
					</div>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="mulai" class="col-sm-4 col-form-label">Mulai Baris</label>
				<div class="col-sm-2">
					<input type="text" id="mulai" name="mulai" class="form-control form-control-sm currency" value="2">
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Exit</button>
			<button type="submit" class="btn btn-success">Save</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$('.select2').select2();
		bsCustomFileInput.init();
		$('.currency').inputmask("numeric", {
            autoUnmask: true,
            radixPoint: ",",
            groupSeparator: ".",
            digits: 2,
            autoGroup: true,
            prefix: '',
            rightAlign: true,
            positionCaretOnClick: true,
        });
		$("#import-form").validate({
			rules: {
				<?php echo $input_file_name?>: {
					required: true,
					accept: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
				},
				int_kategori_ktp_id:{
					required: true,
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI('#modal-import', 'progress', 4);
                //let blc = '#modal-import';
                //blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						setFormMessage('.form-message', data);
						if(data.stat){
							dataTable.draw();
							resetForm(form)
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>