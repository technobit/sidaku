<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script>
 	function get_kelurahan(th,select_id,int_kelurahan_id){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("wilayah/kelurahan_usr")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"), 
					int_kecamatan_id: $('#int_kecamatan_id').val()}
		}).done(function (data){
			refreshToken(data);
			opt = (data.data.length > 0)? '<option value="<?=$kelurahan_usr?>">- Semua Kelurahan - </option>' : opt;
			$.each(data.data, function (i, item) {
				opt += '<option value="' + item.int_kelurahan_id + '">' + item.var_kelurahan + '</option>';
			});
			$('#'+select_id).html(opt).select2();
			if (int_kelurahan_id != '0'){
				$('#'+select_id).val(int_kelurahan_id).trigger("change");
			}

		});
	}
    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#table_data').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                    d.kategori_filter = $('.int_kategori_ktp_id').val();
                    d.kecamatan_filter = $('.kecamatan_filter').val();
					d.kelurahan_filter = $('.kelurahan_filter').val();

                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
				{
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [6],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '';
                                break;
                            case 1:
                                return '<span class="badge bg-danger">Belum Rekam</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-warning">Belum Cetak</span>';
                                break;
                            case 3:
                                return '<span class="badge bg-primary">Belum Ambil</span>';
                                break;
                            default: '';
                        }
                    }
                },
                {
                    "aTargets": [7],
                    "mRender": function(data, type, row, meta) {
                        return  ('<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus data" ><i class="fa fa-trash"></i></a> ');
                    }
                }
            ]
        });

        $('.int_kategori_ktp_id').change(function(){
            dataTable.draw();
        });

        $('.kecamatan_filter').change(function(){
            dataTable.draw();
        });

        $('.kelurahan_filter').change(function(){
            dataTable.draw();
        });

        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>