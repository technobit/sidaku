<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nik extends MX_Controller {

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'MASTER-NIK';
        $this->module   = 'master';
        $this->routeURL = 'master_wilayah';

		$this->load->model('nik_model', 'model');
        $this->config->set_item('compress_output', FALSE);
    }
	
    public function index(){
		return null;
	}

	public function cek($int_kategori_ktp_id){
		$data = $this->model->get_nik($int_kategori_ktp_id, $this->input->post('var_nik', true));
        if(empty($data) || $data[0]->success == 0){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'msg' => 'Data Kependudukan Tidak Ditemukan Atau Sudah Pernah Di-survey',
								'csrf' => [ 'name' => $this->getCsrfName(),
		                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
									'msg' => 'Data Kependudukan Tidak Ditemukan',
                                    'csrf' => [ 'name' => $this->getCsrfName(),
												'token' => $this->getCsrfToken()]),200, false);
								}
    }
	public function check_nik_dokter(){
		$data = $this->model->check_nik_dokter($this->input->post('var_nik', true));
		if(empty($data)){
			$this->set_json(array( 'stat' => FALSE, 
								'msg' => "Data Dokter tidak ditemukan, Silahkan tambahkan data Dokter dengan benar",
								'csrf' => [ 'name' => $this->getCsrfName(),
											'token' => $this->getCsrfToken()]));
		}else{
			$msg = "Data Dokter ditemukan, Anda dapat mengubah data Dokter dan statusnya di Fasilitas Kesehatan";
			$data[0]->save_mode = 1;
			if(isset($data[0]->faskes_dokter_id)){
				$data[0]->save_mode = 0;
				$msg = "Data Dokter sudah terdaftar di Fasilitas Kesehatan";
			}
			$this->set_json(array( 'stat' => TRUE, 
								'data' => $data,
								'msg' => $msg,
								'csrf' => [ 'name' => $this->getCsrfName(),
											'token' => $this->getCsrfToken()]),200, false);
		}
	}
}
