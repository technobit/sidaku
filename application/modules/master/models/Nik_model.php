<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Nik_model extends MY_Model {

	public function get_nik($int_kategori_ktp_id, $var_nik = 0){
		$int_id_user = $this->session->userdata['user_id'];
		return $this->db->query("CALL web_cari_nik_verifikasi({$int_kategori_ktp_id},{$int_id_user},{$var_nik})")->result();
	}

}
