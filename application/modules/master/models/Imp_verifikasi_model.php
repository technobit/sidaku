<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReadFilter;

class Imp_verifikasi_model extends MY_Model {


	public function get_kategori(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_kategori_ktp}
								 ORDER BY int_kategori_ktp_id ASC")->result();
	}

	public function get_list(){
		return $this->db->query("SELECT *
								 FROM	{$this->m_verifikasi_ktp}
								 ORDER BY var_nik ASC")->result();
	}

    public function list($kategori_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0, $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*")
				->from($this->m_verifikasi_ktp." mvk")
				->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
				->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if(($kategori_filter != "")){ // filter
			$this->db->where('mvk.int_kategori_ktp_id', $kategori_filter);
		}
			
		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mvk.int_kecamatan_id', $kecamatan_filter);
		}

		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mvk.int_kelurahan_id', $kelurahan_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mvk.var_nik', $filter)
					->or_like('mvk.var_nama', $filter)
					->or_like('mk.var_kecamatan', $filter)
					->or_like('ml.var_kelurahan', $filter)
					->group_end();
		}

		$order = 'var_nik ';
		switch($order_by){
			case 1 : $order = 'var_nik '; break;
			case 2 : $order = 'var_nama '; break;
			case 4 : $order = 'mk.var_kecamatan'; break;
			case 5 : $order = 'ml.var_kelurahan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kategori_filter = "", $kecamatan_filter = 0, $kelurahan_filter = 0,  $filter = NULL){
		$this->db->select("*")
				->from($this->m_verifikasi_ktp." mvk")
				->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
				->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left");

		if(($kategori_filter != "")){ // filter
			$this->db->where('mvk.int_kategori_ktp_id', $kategori_filter);
		}
			
		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mvk.int_kecamatan_id', $kecamatan_filter);
		}

		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mvk.int_kelurahan_id', $kelurahan_filter);
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('mvk.var_nik', $filter)
			->or_like('mvk.var_nama', $filter)
			->or_like('mk.var_kecamatan', $filter)
			->or_like('ml.var_kelurahan', $filter)
			->group_end();
        }
		return $this->db->count_all_results();
	}

	public function get($int_mverifikasi_ktp_id){
		return $this->db->select("*")
					->get_where($this->m_verifikasi_ktp, ['int_mverifikasi_ktp_id' => $int_mverifikasi_ktp_id])->row();
	}

	public function update($int_mverifikasi_ktp_id, $ins){
		$this->db->trans_begin();

		$this->db->where('int_mverifikasi_ktp_id', $int_mverifikasi_ktp_id);
		$this->db->update($this->m_verifikasi_ktp, $ins);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_mverifikasi_ktp_id){
		$this->db->trans_begin();
		$this->db->delete($this->m_verifikasi_ktp,  ['int_mverifikasi_ktp_id' => $int_mverifikasi_ktp_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
	public function import($in, $file){
		$user 	= $this->session->userdata('username');
		$var_nik = 'A';
		$var_nama = 'B';
		$int_jeniskelamin_id = 'C';
		$var_tempat_lahir = 'D';
		$dt_tanggal_lahir = 'E';
		$var_agama = 'F';
		$int_status_kawin_id = 'G';
		$int_pekerjaan_id = 'H';
		$var_alamat = 'I';
		$int_kecamatan_id = 'J';
		$int_kelurahan_id = 'K';
		$var_rw = 'L';
		$var_rt = 'M';

		$filterSubset = new MyReadFilter($in['mulai'],
						[$var_nik, $var_nama,
						$int_jeniskelamin_id,
						$var_tempat_lahir,
						$dt_tanggal_lahir,
						$var_agama,
						$int_status_kawin_id,
						$int_pekerjaan_id,
						$var_alamat,
						$int_kecamatan_id,
						$int_kelurahan_id,
						$var_rw,
						$var_rt]
					);
		$reader = IOFactory::createReader(ucfirst(ltrim($file['file_ext'],'.')));
		$reader->setReadDataOnly(true);
		$reader->setReadFilter($filterSubset);
		$spreadsheet = $reader->load($file['full_path']);
		$data = $spreadsheet->getActiveSheet()->toArray(null, false, false, true);
		
		//INSERT IGNORE INTO-> 0 row affected if duplicate
		//REPLACE INTO -> replace old duplicate entry
		$ins_verifikasi_ktp = "REPLACE INTO {$this->m_verifikasi_ktp} 
						(`int_kategori_ktp_id`,`var_nik`,`var_nama`,`int_jeniskelamin_id`,`var_tempat_lahir`,`dt_tanggal_lahir`,`var_agama`,`int_status_kawin_id`,`int_pekerjaan_id`,
						`var_alamat`,`int_kecamatan_id`,`int_kelurahan_id`,`var_rw`,`var_rt`) VALUES ";
		$total = 0;
		foreach($data as $i => $d){
			if($i > ($in['mulai'] - 1)){
				$total++;
				

				$strtotime_tanggal_lahir = strtotime($d[$dt_tanggal_lahir]);
				$format_tanggal_lahir = date('Y-m-d',$strtotime_tanggal_lahir);
				
				$d_var_nik = $this->db->escape(trim($d[$var_nik]));
				$d_var_nama = $this->db->escape(trim($d[$var_nama]));
				$d_int_jeniskelamin_id = $this->db->escape(trim($d[$int_jeniskelamin_id]));
				$d_var_tempat_lahir = $this->db->escape(trim($d[$var_tempat_lahir]));
				$d_dt_tanggal_lahir = $this->db->escape(trim($format_tanggal_lahir));
				$d_var_agama = $this->db->escape(trim($d[$var_agama]));
				$d_int_status_kawin_id = $this->db->escape(trim($d[$int_status_kawin_id]));
				$d_int_pekerjaan_id = $this->db->escape(trim($d[$int_pekerjaan_id]));
				$d_var_alamat = $this->db->escape(trim($d[$var_alamat]));
				$d_int_kecamatan_id = $this->db->escape(trim($d[$int_kecamatan_id]));
				$d_int_kelurahan_id = $this->db->escape(trim($d[$int_kelurahan_id]));
				$d_var_rw = $this->db->escape($d[$var_rw]);
				$d_var_rt = $this->db->escape($d[$var_rt]);


				$ins_verifikasi_ktp .= "({$in['int_kategori_ktp_id']}, {$d_var_nik}, {$d_var_nama}, {$d_int_jeniskelamin_id}, {$d_var_tempat_lahir}, {$d_dt_tanggal_lahir}, {$d_var_agama}, {$d_int_status_kawin_id}, 
								{$d_int_pekerjaan_id}, {$d_var_alamat}, {$d_int_kecamatan_id}, {$d_int_kelurahan_id}, {$d_var_rw}, {$d_var_rt}),";
			}
		}
		$this->db->trans_begin();
		
		$this->db->insert($this->h_import, ['username' => $user,
										  'file_name' => $file['orig_name'],
										  'direktori' => $file['full_path'],
										  'total' => $total]);
		
		$ins_verifikasi_ktp = rtrim($ins_verifikasi_ktp, ',').';';
		$this->db->query($ins_verifikasi_ktp);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return $total;
		}
	}
}

class MyReadFilter implements IReadFilter {
    private $startRow = 0;
    private $columns = [];

    public function __construct($startRow, $columns){
        $this->startRow = $startRow;
        $this->columns = $columns;
    }

    public function readCell($column, $row, $worksheetName = ''){
        if ($row >= $this->startRow) {
            if (in_array($column, $this->columns)) {
                return true;
            }
        }
        return false;
    }
}
