<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Auth extends MX_Controller
{
    function __construct(){
        parent::__construct();
        $this->module      = 'api';

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        
        $this->load->model('api/auth_model', 'auth');
    }

    public function login(){
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]');

        if ($this->form_validation->run() == FALSE) {
            $this->set_json(['status' => false,
                'message' => $this->form_validation->error_array()]);
        } else {
            if ($this->auth->getAPILoginCredential($this->input_post('username'), $this->input_post('password'), $this->api_identifier())) {
                $this->set_json(['status' => true, 'message' => $this->auth->getMessage(), 'data' => $this->auth->getAPICredential()]);
            } else {
                $this->set_json(['status' => false, 'message' => $this->auth->getMessage()]);
            }
        }
    }

    public function logout(){
        if(!$this->validateAPIRequest()) return;

        if ($this->auth->getAPILogout($this->api_headers())) {
            $this->set_json(['status' => true, 'message' => 'User telah logout']);
        } else {
            $this->set_json(['status' => false, 'message' => 'Request salah'], 201);
        }
    }

    public function update_password(){
        if(!$this->validateAPIRequest()) return;

        $this->form_validation->set_rules('old_password', 'Password Lama', 'required|min_length[3]');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'required|min_length[3]');

        if ($this->form_validation->run() == FALSE) {
            $this->set_json(['status' => false,
                'message' => $this->form_validation->error_array()]);
        } else {
            if ($this->auth->updateUserPassword($this->api_user(), $this->input_post('old_password'), $this->input_post('new_password'))) {
                $this->set_json(['status' => true, 'message' => $this->auth->getMessage()]);
            } else {
                $this->set_json(['status' => false, 'message' => $this->auth->getMessage()]);
            }
        }
    }
}

