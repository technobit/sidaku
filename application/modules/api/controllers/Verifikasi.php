<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Verifikasi extends MX_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
        
        $this->load->model('api/verifikasi_model', 'verifikasi');
    }

    public function cari_nik(){
        $data = $this->verifikasi->cari_nik($this->input_post('kategori_id'), $this->input_post('user_id'), $this->input_post('nik'));
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data]);
    }

    public function history(){
        if(!$this->validateAPIRequest()) return;

        $user_id    = $this->input_get('user_id');
        $limit      = intval($this->input_get('limit'));
        $offset     = intval($this->input_get('offset'));

        $data = $this->verifikasi->history($user_id, $limit, $offset);

        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'limit' => $limit,
                         'offset' => $offset,
                         'data' => $data], 200, false);
    }

    public function detail_history(){
        if(!$this->validateAPIRequest()) return;

        $verifikasi_id      = intval($this->input_get('verifikasi_id'));
        $kategori_ktp_id    = intval($this->input_get('kategori_ktp_id'));

        $data = $this->verifikasi->detail($kategori_ktp_id, $verifikasi_id);
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function save_brm(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->save_brm($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data]);
    }

    public function save_bck(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->save_bck($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data]);
    }

    public function save_kba(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->save_kba($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data]);
    }

    public function upload_img(){
        if(!$this->validateAPIRequest()) return;
        
		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}

            $config['upload_path']   = $upload_path;
            $config['allowed_types'] = $this->config->item('allowed_types'); 
            $config['encrypt_name']  = $this->config->item('encrypt_name');
            $config['max_size']      = $this->config->item('max_size');

            $this->load->library('upload', $config);
            
                        
            if($this->upload->do_upload('image')){
                $lampiran_insert 	= array_merge($this->upload->data(), ['dir' => $img_dir]);
                $lampiran_msg = ['stat' => true, 'msg' => 'Upload Success'];
            }else{
                $lampiran_msg = ['stat' => false, 'msg' => $this->upload->display_errors('','')];  
            }
            $this->upload->error_msg = [];

            //$files = ['files' => $lampiran_msg];
        
                
        switch ($this->input_post('kategori')) {
            case 'brm':
                $insert = $this->model->insert_img($this->input->post(), $lampiran_insert, 'brm');
                break;
            case 'bck':
                $insert = $this->model->insert_img($this->input->post(), $lampiran_insert, 'bck');
                break;
        }

        $this->set_json(['status' => ($insert)? true : false,
                         'message' => ($insert)? 'Success '.$lampiran_msg : 'Failed '.$lampiran_msg]);

    }

    public function survey_brm(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->survey_brm($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function survey_bck(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->survey_bck($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function survey_kba(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->survey_kba($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function detail_survey_brm(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->detail_survey_brm($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function detail_survey_bck(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->detail_survey_bck($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }

    public function detail_survey_kba(){
        if(!$this->validateAPIRequest()) return;
        
        $data = $this->verifikasi->detail_survey_kba($this->input_post());
        $this->set_json(['status' => ($data)? true : false,
                         'message' => ($data)? 'Success' : 'Failed',
                         'data' => $data], 200, false);
    }
}

