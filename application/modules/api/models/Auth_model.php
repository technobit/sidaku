<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Auth_model extends MY_Model {
    private $apiCredential = array();
    private $message = '';

    public function getAPICredential(){
        return $this->apiCredential;
    }

    public function getMessage(){
        return $this->message;
    }

    public function getAPILoginCredential($username, $password, $identifier = null){
        $return = false;
        $query  = $this->callProcedure("mobile_login(?)", [$username]);
        if($query){
            $now = date("Y-m-d H:i:s");
            $api_expire = $this->db->escape($this->config->item('api_expire'));
            $token = md5($this->config->item('api_prefix') . $query->int_id_user . '@' . $username . $this->config->item('api_suffix') . $identifier . '#' . uniqid('', true));
            $api_identifier = md5('#'.$now.$identifier);
            if ($query->int_status) {
                if (password_verify($password, $query->txt_password)) {

                    $token = md5($this->config->item('api_prefix') . $query->int_id_user . '@' . $username . $this->config->item('api_suffix') . $identifier . '#' . uniqid('', true));
                    $now = date("Y-m-d H:i:s");
                    $this->callSimpleProcedure("mobile_set_token({$query->int_id_group}, {$query->int_id_user}, {$this->db->escape($username)}, '{$token}', '{$api_identifier}', {$api_expire})");

                    $this->apiCredential = ['user_id' => strtolower($query->int_id_user),
                        'username' => $username,
                        'name' => $query->txt_nama_depan.' '.$query->txt_nama_belakang,
                        'token' => $token,
                        'identifier' => $api_identifier,
                        //'expired_date' => $expired_date,
                        'group' => $query->txt_group];
                    $this->message = 'Login sukses. ';
                    $return = true;
                } else {
                    $this->message = 'Username dan Password salah.';
                }
            } else {
                $this->message = 'User tidak aktif.';
            }
        } else {
            $this->message = 'Username dan Password salah';
        }
        return $return;
    }

    public function validateAPIToken($user_id = null, $identifier = null, $token = null){
        if(!$user_id || !$token){
            $this->message = 'User dan Token tidak valid.';
            return false;
        }
        $check = $this->callProcedure("mobile_validate_token({$user_id}, '{$identifier}', '{$token}')");
        if($check->total > 0){
            return true;
        } else {
            $this->message = ($check->total == 0)? 'User dan Token telah kadaluarsa. Silahkan login kembali.' : '';
            return false;
        }
    }

    public function getAPILogout($headers = null){
        if(is_object($headers)){
            $this->callSimpleProcedure("mobile_logout({$headers->user_id}, '{$headers->identifier}', '{$headers->token}')");
            return true;
        }
        return false;
    }


    public function updateUserPassword($user_id, $old_password, $new_password){
        $this->message = 'User_id tidak valid.';
        $query = $this->callProcedure("mobile_getpassword({$user_id})");

        if($query) {
            if (password_verify($old_password, $query->password)) {
                $this->callSimpleProcedure("mobile_update_password({$user_id}, '{$this->db->escape(password_hash($new_password, PASSWORD_BCRYPT))}')");
                $this->message = 'Password berhasil di-update.';
                return true;
            }else{
                $this->message = 'Password Lama salah.';
            }
        }
        return false;
    }
}
