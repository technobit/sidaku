<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Verifikasi_model extends MY_Model {

    public function cari_nik($kategori_ktp_id, $user_id, $var_nik){
        return $this->callProcedure("mobile_cari_nik_verifikasi(?,?,?)", [$kategori_ktp_id, $user_id, $var_nik]);
    }

    public function history($user_id, $limit = 0, $ofset = 0){
        return $this->callProcedure("mobile_get_history_verifikasi(?, ?, ?)", [$user_id, $limit, $ofset], 'result');
    }

    public function detail($kategori_ktp_id, $verifikasi_id){
        return $this->callProcedure("mobile_get_detail_history_verifikasi(?,?)", [$kategori_ktp_id, $verifikasi_id]);
    }

    public function save_brm($ins){
        return $this->callProcedure("mobile_verifikasi_brm_insert(?,?,?,?,?,?,?,?,?,?)", 
                                        [$ins['user_id'], 
                                        $ins['nik'],
                                        $ins['nama'],
                                        $ins['nohp'],
                                        $ins['jawaban_1'],
                                        $ins['jawaban_2'],
                                        $ins['jawaban_3'],
                                        $ins['int_lokasi_perekaman'],
                                        $ins['dt_perekaman'],
                                        $ins['keterangan']]);
    }

    public function save_bck($ins){
        return $this->callProcedure("mobile_verifikasi_bck_insert(?,?,?,?,?,?)", 
                                        [$ins['user_id'], 
                                        $ins['nik'],
                                        $ins['nama'],
                                        $ins['nohp'],
                                        $ins['jawaban_1'],
                                        $ins['keterangan']]);
    }

    public function save_kba($ins){
        return $this->callProcedure("mobile_verifikasi_kba_insert(?,?,?,?,?,?,?)", 
                                        [$ins['user_id'], 
                                        $ins['nik'],
                                        $ins['nama'],
                                        $ins['nohp'],
                                        $ins['jawaban_1'],
                                        $ins['dt_perekaman'],
                                        $ins['keterangan']]);
    }

    public function insert_img($ins, $image, $table){
		$this->db->trans_begin();
        
        $insert['int_verifikasi_brm_id'] = $ins['verifikasi_id'];
        $insert['var_jenis_img'] = $this->db->escape($image['dir'].'/'.$image['file_name']);
        $insert['var_image'] = $this->db->escape($ins['jenis_img']);

        $this->callProcedure("mobile_verifikasi_".$table."_upload(?,?,?,?)", 
                            [$ins['int_verifikasi_brm_id'], 
                            $ins['var_jenis_img'],
                            $ins['var_image']]);
		$this->setQueryLog('ins_image');

        if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
    }
    
    public function survey_brm($post){
        return $this->callProcedure("mobile_get_datasurvey_brm(?)", 
                                        [$post['user_id']], 'result');
    }

    public function survey_bck($post){
        return $this->callProcedure("mobile_get_datasurvey_bck(?)", 
                                        [$post['user_id']], 'result');
    }

    public function survey_kba($post){
        return $this->callProcedure("mobile_get_datasurvey_kba(?)", 
                                        [$post['user_id']], 'result');
    }

    public function detail_survey_brm($post){
        return $this->callProcedure("mobile_get_datasurvey_brm_detail(?,?)",
                                    [$post['user_id'], $post['int_verifikasi_id']]);
    }

    public function detail_survey_bck($post){
        return $this->callProcedure("mobile_get_datasurvey_bck_detail(?,?)", 
                                    [$post['user_id'], $post['int_verifikasi_id']]);
    }

    public function detail_survey_kba($post){
        return $this->callProcedure("mobile_get_datasurvey_kba_detail(?,?)", 
                                    [$post['user_id'], $post['int_verifikasi_id']]);
    }
}
