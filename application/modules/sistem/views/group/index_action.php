<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="group-form" width="80%">
<div id="modal-group" class="modal-dialog modal-md" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="nama" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-10">
					<input type="text" class="form-control form-control-sm" id="nama" placeholder="Nama" name="nama" value="<?=isset($data->nama)? $data->nama : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10 mt-1">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="is_aktif" value="1" <?=isset($data->is_aktif)? (($data->is_aktif == 1)? 'checked' : '') : '' ?>>
							<label for="radioPrimary1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="radioPrimary2" name="is_aktif" value="0" <?=isset($data->is_aktif)? (($data->is_aktif == 0)? 'checked' : '') : 'checked' ?>>
						<label for="radioPrimary2">Non-aktif</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-warning">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	$(document).ready(function(){
		$("#group-form").validate({
			rules: {
				nama: {
					required: true,
					minlength: 5,
					maxlength: 50
				},
				is_aktif: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#group-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>