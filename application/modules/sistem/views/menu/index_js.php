<script>
    var dataTable;
    $(document).ready(function() {
        $('.level_filter, .parent_filter').select2();

        dataTable = $('#table_menu').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                    d.level  = $('.level_filter').val();
                    d.parent = $('.parent_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "5%",
                    "sClass": "center",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "20%"
                },
                {
                    "sWidth": "20%"
                },
				{
                    "sWidth": "10%"
                },
				{
                    "sWidth": "10%",
                    "bSearchable": false,
                    "sClass": "d-none d-sm-table-cell "
                },
                {
                    "sWidth": "4%",
                    "bSearchable": false,
                    "sClass": "d-none d-sm-table-cell "
                },
                {
                    "sWidth": "4%",
                    "bSearchable": false,
                    "sClass": "d-none d-sm-table-cell "
                },
                {
                    "sWidth": "14%",
                    "bSearchable": false,
                    "sClass": "d-none d-sm-table-cell "
                },
                {
                    "sWidth": "5%",
                    "bSearchable": false,
                    "sClass": "d-none d-sm-table-cell "
                },
                {
                    "sWidth": "8%",
                    "bSortable": false,
                    "bSearchable": false
                }
            ],
            /*"fnDrawCallback": function ( nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            	$( 'a', this.fnGetNodes() ).tooltip();
            },*/
            "aoColumnDefs": [{
                    "aTargets": [8],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '<span class="badge bg-danger">non-aktif</span>';
                                break;
                            case 1:
                                return '<span class="badge bg-success">aktif</span>';
                                break;
                        }
                    }
                },
                {
                    "aTargets": [9],
                    "mRender": function(data, type, row, meta) {
                        return  '<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit Menu" ><i class="fa fa-edit"></i></a> ' +
                                '<a href="#" data-block="body" data-url="<?=site_url("{$routeURL}/") ?>' + data + '/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus Menu" ><i class="fa fa-trash"></i></a>';
                    }
                }
            ]
        });

        $('.dataTables_filter input')
            .unbind()
            .bind('keyup', function(e) {
                if (e.keyCode == 13) {
                    dataTable.search($(this).val()).draw();
                }
            });

        $('.level_filter, .parent_filter').change(function(){
            dataTable.draw();
        });
    });
</script>