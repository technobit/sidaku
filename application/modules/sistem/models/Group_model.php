<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Group_model extends MY_Model {
    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$order_by   = strtolower($order_by); 
		$sort       = (strtolower(trim($sort)) == 'asc')? 'ASC' : 'DESC';

		$this->db->select("int_id_group as group_id, txt_nama as nama, is_active as is_aktif")
					->from($this->s_group);

		if(!empty($filter)){ // filters 
			$filter = $this->filterAlphaNumeric($filter);
			$this->db->like('txt_nama', $filter);
		}

		$order = 'txt_nama ';
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->s_group);

		if(!empty($filter)){ // filters 
	        $filter = $this->filterAlphaNumeric($filter);
            $this->db->like('txt_nama', $filter);
		}
		return $this->db->count_all_results();
	}


	
	public function create($in){
		$col['txt_nama']		= $in['nama'];
		$col['is_active']	= $in['is_aktif'];
		$this->db->insert($this->s_group, $col);
	}

	public function get($group_id){
		return $this->db->query("	SELECT 	int_id_group as group_id, txt_nama as nama, is_active as is_aktif
									FROM	{$this->s_group}  
									WHERE	int_id_group = ?", [$group_id])->row();
	}

	public function update($group_id, $in){
        $col['txt_nama']	= $in['nama'];
        $col['is_active']	= $in['is_aktif'];

        $this->db->trans_begin();
		$this->db->where('int_id_group', $group_id);
		$this->db->update($this->s_group, $col);

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($group_id){
		$this->db->trans_begin();
		$this->db->query("DELETE FROM {$this->s_group} WHERE int_id_group = ?",[$group_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}
}