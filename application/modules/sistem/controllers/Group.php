<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Group extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'GROUP'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_group';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('group_model', 'group');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Users Group';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_group';
		$this->breadcrumb->title = 'Users Group';
		$this->breadcrumb->list = ['Sistem', 'Group'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");	
		$this->render_view('group/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->group->listCount($this->input->post('search[value]', TRUE));
		$ldata = $this->group->list($this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

		$i 	   = $this->input->post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->nama, $d->is_aktif, $d->group_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['group'] = $this->group->list();
		$data['url']   = site_url("{$this->routeURL}/save");
		$data['title'] = 'Tambah data group';
		$this->load_view('group/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->group->create($this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($group_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->group->get($group_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['group']  = $this->group->list();
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$group_id");
			$data['title']	= 'Edit data group';
			$this->load_view('group/index_action', $data);
		}
		
	}

	public function update($group_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false, 
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->group->update($group_id, $this->input->post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => ($check)? "Data berhasil di-update" : "Terjadi kesalahan teknis",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($group_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->group->get($group_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$group_id/del");
			$data['title']	= 'Hapus data group';
			$data['info']	= [	'Nama' => $res->nama];
			$this->load_view('group/index_delete', $data);
		}
	}

	public function delete($group_id){
		$this->authCheckDetailAccess('d');

		$check = $this->group->delete($group_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data berhasil dihapus" : "Data tidak dapat dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}

	public function menu_get($group_id){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$this->load->model('menu_model', 'menu');
				
		$data['table'] 	= $this->menu->getMapMenu($group_id);
		$data['url']	= site_url("{$this->routeURL}/$group_id/menu");
		$data['title']	= 'Manage group menu';
		$this->load_view('group/index_menu', $data);		
	}


	public function menu_save($group_id){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$this->load->model('menu_model', 'menu');
		$check = $this->menu->setGroupMenu($group_id, $this->input->post());
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Group Menu berhasil diperbarui" : "Data tidak dapat diperbarui",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}
}
