<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Menu extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'MENU'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_menu';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('menu_model', 'menu');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'System Menu';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_menu';
		$this->breadcrumb->title = 'System Menu';
		$this->breadcrumb->list = ['Sistem', 'Menu'];
		$this->js = true;
		$data['level'] 	= $this->menu->getLevelMenu();
		$data['parent'] = $this->menu->getParentMenu();
		$data['url'] 	= site_url("{$this->routeURL}/add");	
		$this->render_view('menu/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r');

		$level  = $this->input_post('level');
		$parent = $this->input_post('parent');

		$data  = array();
		$total = $this->menu->listCount($level, $parent, $this->input->post('search[value]', TRUE));
		$ldata = $this->menu->list($level, $parent, $this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

		$i 	   = $this->input->post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->kode, $d->nama, $d->url, $d->class, $d->level, $d->urutan, $d->parent_name, $d->is_aktif, $d->menu_id);
		}
		$this->set_json(array( 'stat' => TRUE, 
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['menu'] = $this->menu->getParentMenu();
		$data['url']   = site_url("{$this->routeURL}/save");
		$data['title'] = 'Tambah data menu';
		$this->load_view('menu/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

        $this->form_validation->set_rules('kode', 'Kode', "required|is_unique[{$this->menu->s_menu}.txt_kode]|min_length[3]|max_length[20]");
		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('url', 'URL', 'min_length[3]|max_length[50]');
		$this->form_validation->set_rules('level', 'Level', 'required|integer|max_length[3]');
		$this->form_validation->set_rules('urutan', 'Urutan', 'required|integer|max_length[4]');
		$this->form_validation->set_rules('class', 'Class', 'required|max_length[20]');
		$this->form_validation->set_rules('icon', 'Icon', 'required|max_length[50]');
		$this->form_validation->set_rules('parent', 'Parent', 'integer');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $this->menu->create($this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => false, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($menu_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->menu->get($menu_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['menu']  = $this->menu->getParentMenu();
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$menu_id");
			$data['title']	= 'Edit data menu';
			$this->load_view('menu/index_action', $data);
		}
		
	}

	public function update($menu_id){
		$this->authCheckDetailAccess('u');
		
		$this->form_validation->set_rules('nama', 'Nama', 'required|min_length[3]|max_length[50]');
		$this->form_validation->set_rules('url', 'URL', 'min_length[3]|max_length[50]');
		$this->form_validation->set_rules('level', 'Level', 'required|integer|max_length[3]');
		$this->form_validation->set_rules('urutan', 'Urutan', 'required|integer|max_length[4]');
		$this->form_validation->set_rules('class', 'Class', 'required|max_length[20]');
		$this->form_validation->set_rules('icon', 'Icon', 'required|max_length[50]');
		$this->form_validation->set_rules('parent', 'Parent', 'integer');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false, 
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->menu->update($menu_id, $this->input->post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => ($check)? "Data berhasil di-update" : "Terjadi kesalahan teknis",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($menu_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->menu->get($menu_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$menu_id/del");
			$data['title']	= 'Hapus data menu';
			$data['info']	= [	'Kode' => $res->kode, 
								'Nama' => $res->nama];
			$this->load_view('menu/index_delete', $data);
		}
	}

	public function delete($menu_id){
		$this->authCheckDetailAccess('d');

		$check = $this->menu->delete($menu_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data berhasil dihapus" : "Data tidak dapat dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
