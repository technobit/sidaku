<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class User extends MX_Controller {

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'USER-ADMIN'; // kode menu pada tabel menu, 1 menu : 1 controller
		$this->module 	= 'sistem';
		$this->routeURL	= 's_user';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('user_model', 'user');
		$this->load->model('group_model', 'group');
		$this->load->model('master/wilayah_model', 'wilayah');
    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'User Admin';
		$this->page->menu 	  = 'sistem';
		$this->page->submenu1 = 's_user';
		$this->breadcrumb->title = 'User Admin';
		$this->breadcrumb->list = ['Sistem', 'User Admin'];
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");	
		$this->render_view('user/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->user->listCount($this->input->post('search[value]', TRUE));
		$ldata = $this->user->list($this->input->post('search[value]', TRUE), $this->input->post('order[0][column]', true), $this->input->post('order[0][dir]'), $this->input->post('length', true), $this->input->post('start', true));

		$i 	   = $this->input->post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->username, $d->nama, $d->grup, $d->is_aktif, $d->user_id);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup

		$data['group'] = $this->group->list();
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan();
		$data['url']   = site_url("{$this->routeURL}/save");
		$data['title'] = 'Tambah data user';
		$this->load_view('user/index_action', $data, true);
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$this->form_validation->set_rules('group_id', 'Group', 'required|integer');
        $this->form_validation->set_rules('username', 'Username', "required|is_unique[{$this->user->s_user}.txt_username]|min_length[3]|max_length[20]");
		$this->form_validation->set_rules('first_name', 'Nama Depan', 'required|min_length[4]|max_length[50]');
		$this->form_validation->set_rules('last_name', 'Nama Belakang', 'min_length[4]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[20]');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $query = $this->user->create($this->input->post());
			$this->set_json([  'stat' => true, 
								'mc' => $query, //modal close
								'msg' => "Data berhasil dibuat",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($user_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->user->get($user_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['group']  = $this->group->list();
			$data['list_kecamatan'] = $this->wilayah->get_kecamatan();
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$user_id");
			$data['title']	= 'Edit data user';
			$this->load_view('user/index_action', $data);
		}
		
	}

	public function update($user_id){
		$this->authCheckDetailAccess('u');

		$this->form_validation->set_rules('group_id', 'Group', 'required|integer');
		$this->form_validation->set_rules('username', 'Username', "required|is_unique_update[{$this->user->s_user}.txt_username.int_id_user.{$user_id}]|min_length[3]|max_length[20]");
        $this->form_validation->set_rules('first_name', 'Nama Depan', 'required|min_length[4]|max_length[50]');
        $this->form_validation->set_rules('last_name', 'Nama Belakang', 'min_length[4]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password', 'min_length[4]|max_length[20]');
		$this->form_validation->set_rules('is_aktif', 'Status', 'required');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false, 
								'msg' => "Terjadi kesalahan",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            $check = $this->user->update($user_id, $this->input->post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => ($check)? "Data berhasil di-update" : "Terjadi kesalahan teknis",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}

	public function confirm($user_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->user->get($user_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Terjadi Kesalahan.', 'message' => 'Data yang dicari tidak ditemukan. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$user_id/del");
			$data['title']	= 'Hapus data user';
			$data['info']	= [	'Username' => $res->username, 
								'Nama' => $res->first_name.' '.$res->last_name];
			$this->load_view('user/index_delete', $data);
		}
	}

	public function delete($user_id){
		$this->authCheckDetailAccess('d');

		$check = $this->user->delete($user_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data berhasil dihapus" : "Data tidak dapat dihapus",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
		
	}
}
