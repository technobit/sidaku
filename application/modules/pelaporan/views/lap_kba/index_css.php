<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/summernote/summernote-bs4.css">
<link rel="stylesheet" href="<?=base_url() ?>assets/plugins/lightGallery/dist/css/lightgallery.css">
<style>
.img-thumb-sm {
    max-width: 250px;
    max-height: 250px;
}

.img-thumb{
    width:100%;
    height:150px;
    border:0 solid rgba(0,0,0,.125);
    border-radius:3px;
    padding:5px;
    box-shadow: 0 0 1px rgba(0,0,0,.125),0 1px 3px rgba(0,0,0,.2);
}
</style>