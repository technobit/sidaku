<<<<<<< HEAD
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_ver_rek extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-VER-REK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_ver_rek';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_ver_rek_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Rekapitulasi Verifikasi KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_ver_rek';
		$this->breadcrumb->title = 'Rekapitulasi Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Rekapitulasi';
		$this->breadcrumb->icon = 'fas fa-file-alt';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Rekapitulasi'];
		$this->css = true;
		$this->js = true;
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_ver_rek/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		//$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_kecamatan, $d->var_kelurahan, $d->belum_perekaman, $d->belum_cetak, $d->belum_ambil);
		}
		$this->set_json(array( 'stat' => TRUE,
								//'iTotalRecords' => $total,
								//'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function export(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $filename = 'Rekapitulasi Verifikasi Data KTP - '.idn_date(date("Y-M-d")).'.xlsx';
        $title    = 'Rekapitulasi';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $judul = 'Rekapitulasi Verifikasi Data KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A3', 'Tanggal:')
            ->setCellValue('C3', idn_date(date("Y-M-d")))
            ->setCellValue('A4', 'NO')
            ->setCellValue('B4', 'KECAMATAN')
            ->setCellValue('C4', 'KELURAHAN')
            ->setCellValue('D4', 'BELUM PEREKAMAN')
            ->setCellValue('E4', 'BELUM CETAK')
            ->setCellValue('F4', 'BELUM AMBIL');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:F4')->getFont()->setBold( true );
        $sheet->getStyle('A4:F4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2f2');

        $i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;
            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_kecamatan);
            $sheet->setCellValue('C'.$x, $d->var_kelurahan);
            $sheet->setCellValue('D'.$x, $d->belum_perekaman);
            $sheet->setCellValue('E'.$x, $d->belum_cetak);
            $sheet->setCellValue('F'.$x, $d->belum_ambil);
        }
        $i++;
        $x++;

        $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');

        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }
}
=======
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_ver_rek extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-VER-REK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_ver_rek';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_ver_rek_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Rekapitulasi Verifikasi KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_ver_rek';
		$this->breadcrumb->title = 'Rekapitulasi Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Rekapitulasi';
		$this->breadcrumb->icon = 'fas fa-file-alt';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Rekapitulasi'];
		$this->css = true;
		$this->js = true;
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_ver_rek/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		//$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_kecamatan, $d->var_kelurahan, $d->belum_perekaman, $d->belum_cetak, $d->belum_ambil);
		}
		$this->set_json(array( 'stat' => TRUE,
								//'iTotalRecords' => $total,
								//'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function export(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $filename = 'Rekapitulasi Verifikasi Data KTP - '.idn_date(date("Y-M-d")).'.xlsx';
        $title    = 'Rekapitulasi';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $judul = 'Rekapitulasi Verifikasi Data KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A3', 'Tanggal:')
            ->setCellValue('C3', idn_date(date("Y-M-d")))
            ->setCellValue('A4', 'NO')
            ->setCellValue('B4', 'KECAMATAN')
            ->setCellValue('C4', 'KELURAHAN')
            ->setCellValue('D4', 'BELUM PEREKAMAN')
            ->setCellValue('E4', 'BELUM CETAK')
            ->setCellValue('F4', 'BELUM AMBIL');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:F4')->getFont()->setBold( true );
        $sheet->getStyle('A4:F4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2f2');

        $i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;
            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_kecamatan);
            $sheet->setCellValue('C'.$x, $d->var_kelurahan);
            $sheet->setCellValue('D'.$x, $d->belum_perekaman);
            $sheet->setCellValue('E'.$x, $d->belum_cetak);
            $sheet->setCellValue('F'.$x, $d->belum_ambil);
        }
        $i++;
        $x++;

        $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');

        foreach(range('A','F') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }
}
>>>>>>> b4148bf50887d3bc96873404ee95799aa8a61f6a
