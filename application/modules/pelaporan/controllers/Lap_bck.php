<<<<<<< HEAD
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_bck extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-BCK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_bck';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_bck_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pelaporan Belum Cetak KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_bck';
		$this->breadcrumb->title = 'Pelaporan Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Belum Cetak KTP';
		$this->breadcrumb->icon = 'fas fa-print';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Belum Cetak'];
		$this->css = true;
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_bck/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_nohp, $d->int_jawaban_1, $d->created, $d->updated);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $filename = 'Laporan BCK - '.idn_date(date("Y-M-d")).'.xlsx';
        $title    = 'BCK';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

        //$ldata = $this->report->list($filter_start, $filter_end, $is_order, $this->input->post('search_key', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $judul = 'Laporan Belum Cetak KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A3', 'Tanggal:')
            ->setCellValue('C3', idn_date(date("Y-M-d")))
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'NIK')
            ->setCellValue('C4', 'NAMA')
            ->setCellValue('D4', 'KELURAHAN')
            ->setCellValue('E4', 'KECAMATAN')
            ->setCellValue('F4', 'STATUS KTP')
            ->setCellValue('G4', 'STATUS VERIFIKASI')
            ->setCellValue('H4', 'USER');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:H4')->getFont()->setBold( true );
        $sheet->getStyle('A4:H4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2f2');

        $i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;

			if(isset($d->int_verifikasi_kba_id)){
				$status_verifikasi = 'Sudah';
			}else{
				$status_verifikasi = 'Belum';
			}

			switch ($d->int_jawaban_1) {
				case 1:
					$ktp = 'Belum'; break;
				case 2:
					$ktp = 'Sudah';  break;
				default:
					$ktp = '';
			}

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_nik);
			$sheet->getStyle('B'.$x)->getNumberFormat()->setFormatCode('################');
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_kelurahan);
            $sheet->setCellValue('E'.$x, $d->var_kecamatan);
            $sheet->setCellValue('F'.$x, $ktp);
            $sheet->setCellValue('G'.$x, $status_verifikasi);
            $sheet->setCellValue('H'.$x, $d->created);
        }
        $i++;
        $x++;

        $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');

        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }
}
=======
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class Lap_bck extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-BCK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_bck';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_bck_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pelaporan Belum Cetak KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_bck';
		$this->breadcrumb->title = 'Pelaporan Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Belum Cetak KTP';
		$this->breadcrumb->icon = 'fas fa-print';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Belum Cetak'];
		$this->css = true;
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$data['url_export'] = site_url("export/{$this->routeURL}");
		$this->render_view('lap_bck/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_nohp, $d->int_jawaban_1, $d->created, $d->updated);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()),200,false);
	}

	public function export(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $filename = 'Laporan BCK - '.idn_date(date("Y-M-d")).'.xlsx';
        $title    = 'BCK';

        /*list($start, $end) = explode(' ~ ', $this->input->post('tanggal', true));
        $filter_start = convertValidDate($start, 'd-m-Y', 'Y-m-d');
        $filter_end	  = convertValidDate($end, 'd-m-Y', 'Y-m-d');*/

        //$ldata = $this->report->list($filter_start, $filter_end, $is_order, $this->input->post('search_key', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

        $judul = 'Laporan Belum Cetak KTP';

        $spreadsheet = new Spreadsheet();
        $spreadsheet->setActiveSheetIndex(0)
            ->setCellValue('A1', $judul)
            ->setCellValue('A3', 'Tanggal:')
            ->setCellValue('C3', idn_date(date("Y-M-d")))
            ->setCellValue('A4', 'No')
            ->setCellValue('B4', 'NIK')
            ->setCellValue('C4', 'NAMA')
            ->setCellValue('D4', 'KELURAHAN')
            ->setCellValue('E4', 'KECAMATAN')
            ->setCellValue('F4', 'STATUS KTP')
            ->setCellValue('G4', 'STATUS VERIFIKASI')
            ->setCellValue('H4', 'USER');

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->mergeCells('A1:I1');
        $sheet->getStyle('A1')->getAlignment()->setHorizontal('center');
        $sheet->getStyle('A1')->getFont()->setBold( true );
        $sheet->getStyle('A3')->getFont()->setBold( true );
        $sheet->getStyle('A4:H4')->getFont()->setBold( true );
        $sheet->getStyle('A4:H4')->getFill()->setFillType('solid')->getStartColor()->setARGB('f2f2f2f2');

        $i = 0;
        $x = 4;
        $total = 0;
        $tra   = 0;
        foreach($ldata as $d){
            $i++;
			$x++;

			if(isset($d->int_verifikasi_kba_id)){
				$status_verifikasi = 'Sudah';
			}else{
				$status_verifikasi = 'Belum';
			}

			switch ($d->int_jawaban_1) {
				case 1:
					$ktp = 'Belum'; break;
				case 2:
					$ktp = 'Sudah';  break;
				default:
					$ktp = '';
			}

            $sheet->setCellValue('A'.$x, $i);
            $sheet->setCellValue('B'.$x, $d->var_nik);
			$sheet->getStyle('B'.$x)->getNumberFormat()->setFormatCode('################');
            $sheet->setCellValue('C'.$x, $d->var_nama);
            $sheet->setCellValue('D'.$x, $d->var_kelurahan);
            $sheet->setCellValue('E'.$x, $d->var_kecamatan);
            $sheet->setCellValue('F'.$x, $ktp);
            $sheet->setCellValue('G'.$x, $status_verifikasi);
            $sheet->setCellValue('H'.$x, $d->created);
        }
        $i++;
        $x++;

        $sheet->getStyle('A'.$x)->getAlignment()->setHorizontal('right');

        foreach(range('A','H') as $columnID) {
            $sheet->getColumnDimension($columnID)->setAutoSize(true);
        }

        $sheet->setTitle($title);
        $spreadsheet->setActiveSheetIndex(0);

        $this->set_header($filename);

		$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
		if (ob_get_contents()) ob_end_clean();
		$writer->save('php://output');
		exit;
    }
}
>>>>>>> b4148bf50887d3bc96873404ee95799aa8a61f6a
