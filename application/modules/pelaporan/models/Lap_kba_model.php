<<<<<<< HEAD
<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lap_kba_model extends MY_Model {

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
						CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->t_verifikasi_kba." tvb")
					->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik", "left")
					->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left")
					->join($this->s_user." suc", "tvb.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "tvb.updated_by = suu.int_id_user", "left");;

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tvb.var_nik', $filter)
					->or_like('var_nama', $filter)
					->group_end();
		}

		$order = 'tvb.var_nik ';
		switch($order_by){
			case 1 : $order = 'tvb.var_nik '; break;
			case 2 : $order = 'tvb.var_nama '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->t_verifikasi_kba);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_nik', $filter)
			->or_like('var_nama', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

}
=======
<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lap_kba_model extends MY_Model {

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
						CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->t_verifikasi_kba." tvb")
					->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik", "left")
					->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left")
					->join($this->s_user." suc", "tvb.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "tvb.updated_by = suu.int_id_user", "left");;

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tvb.var_nik', $filter)
					->or_like('var_nama', $filter)
					->group_end();
		}

		$order = 'tvb.var_nik ';
		switch($order_by){
			case 1 : $order = 'tvb.var_nik '; break;
			case 2 : $order = 'tvb.var_nama '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->t_verifikasi_kba);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_nik', $filter)
			->or_like('var_nama', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

}
>>>>>>> b4148bf50887d3bc96873404ee95799aa8a61f6a
