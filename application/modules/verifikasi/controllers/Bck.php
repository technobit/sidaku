<<<<<<< HEAD
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Bck extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'VER-BCK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'verifikasi';
		$this->routeURL = 'ver_bck';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('ver_bck_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Verifikasi Data KTP';
		$this->page->menu 	  = 'verifikasi';
		$this->page->submenu1 = 'ver_bck';
		$this->breadcrumb->title = 'Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Verifikasi Data Cetak KTP';
		$this->breadcrumb->icon = 'fas fa-print';
		$this->breadcrumb->list = ['Verifikasi Data KTP', 'Belum Cetak KTP'];
		$this->css = true;
		$this->js = true;
		$data['kecamatan_usr'] = $this->session->userdata['int_kecamatan_id'];
		$data['kelurahan_usr'] = $this->session->userdata['int_kelurahan_id'];
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan_usr();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('bck/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$action = '<div style="width:50px;display:inline-block">';
			if(isset($d->int_verifikasi_bck_id)){
				if($this->authCheckDetailAccess('u', true)){
					$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_verifikasi_bck_id.'" class="ajax_modal btn btn-xs btn-primary tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a> ';
				}
				if($this->authCheckDetailAccess('d', true)){
					$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_verifikasi_bck_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" ><i class="fa fa-trash"></i></a>';
				}			}else{
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_mverifikasi_ktp_id.'/add" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a>';
			}
			$action .= '</div>';

			$data[] = array($i.'.', $d->var_nik, $d->var_nama, $d->var_tempat_lahir.', '.(idn_date($d->dt_tanggal_lahir, "j F Y")), $d->var_kecamatan, $d->var_kelurahan, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add($int_mverifikasi_ktp_id){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$res = $this->model->add($int_mverifikasi_ktp_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data']			= $res;
			$data['url']			= site_url("{$this->routeURL}/save");
			$data['title']			= 'Form BCK';
			$data['input_file_name']= $this->input_file_name;
			$this->load_view('bck/index_action', $data, true);
		}		
	}


	public function save(){
		$this->authCheckDetailAccess('c');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
            $check = $this->model->create($this->input->post(), $lampiran_insert);
           // $this->model->create($this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_verifikasi_bck_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_verifikasi_bck_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_verifikasi_bck_id");
			$data['title']	= 'Edit BCK';
			$data['input_file_name'] = $this->input_file_name;
			$this->load_view('bck/index_action', $data);
		}
		
	}

	public function update($int_verifikasi_bck_id){
		$this->authCheckDetailAccess('u');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				$check = $this->model->update($int_verifikasi_bck_id, $this->input->post(), $lampiran_insert);
			}else{
				$check = $this->model->update($int_verifikasi_bck_id, $this->input->post(), null);

			}
			
			$this->set_json(array_merge([ 'stat' => $check, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}

	public function confirm($int_verifikasi_bck_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_verifikasi_bck_id, false);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_verifikasi_bck_id/del");
			$data['title']	= 'Hapus Data KTP';
			$data['info']   = [ 'NIK' => $res->var_nik,
                               'Nama' => $res->var_nama,
                               'Tanggal Lahir' => $res->dt_tanggal_lahir];
			$this->load_view('bck/index_delete', $data);
		}
	}

	public function delete($int_verifikasi_bck_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_verifikasi_bck_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}

	public function confirm_del_lampiran($int_verifikasi_bck_img_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_verifikasi_bck_img_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("ver_bck/$int_verifikasi_bck_img_id/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('bck/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_verifikasi_bck_img_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_verifikasi_bck_img_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}	
}

=======
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Bck extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'VER-BCK'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'verifikasi';
		$this->routeURL = 'ver_bck';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('ver_bck_model', 'model');
		$this->load->model('master/wilayah_model', 'wilayah');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Verifikasi Data KTP';
		$this->page->menu 	  = 'verifikasi';
		$this->page->submenu1 = 'ver_bck';
		$this->breadcrumb->title = 'Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Verifikasi Data Cetak KTP';
		$this->breadcrumb->icon = 'fas fa-print';
		$this->breadcrumb->list = ['Verifikasi Data KTP', 'Belum Cetak KTP'];
		$this->css = true;
		$this->js = true;
		$data['kecamatan_usr'] = $this->session->userdata['int_kecamatan_id'];
		$data['kelurahan_usr'] = $this->session->userdata['int_kelurahan_id'];
		$data['list_kecamatan'] = $this->wilayah->get_kecamatan_usr();
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('bck/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input->post('kecamatan_filter', true), $this->input->post('kelurahan_filter', true), $this->input->post('status_filter', true), $this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			$action = '<div style="width:50px;display:inline-block">';
			if(isset($d->int_verifikasi_bck_id)){
				if($this->authCheckDetailAccess('u', true)){
					$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_verifikasi_bck_id.'" class="ajax_modal btn btn-xs btn-primary tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a> ';
				}
				if($this->authCheckDetailAccess('d', true)){
					$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_verifikasi_bck_id.'/del" class="ajax_modal btn btn-xs btn-danger tooltips" data-placement="top" data-original-title="Hapus" ><i class="fa fa-trash"></i></a>';
				}			}else{
				$action .= '<a href="#" data-block="body" data-url="'.site_url("{$this->routeURL}/").$d->int_mverifikasi_ktp_id.'/add" class="ajax_modal btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Edit" ><i class="fa fa-edit"></i></a>';
			}
			$action .= '</div>';

			$data[] = array($i.'.', $d->var_nik, $d->var_nama, $d->var_tempat_lahir.', '.(idn_date($d->dt_tanggal_lahir, "j F Y")), $d->var_kecamatan, $d->var_kelurahan, $action);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add($int_mverifikasi_ktp_id){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$res = $this->model->add($int_mverifikasi_ktp_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data']			= $res;
			$data['url']			= site_url("{$this->routeURL}/save");
			$data['title']			= 'Form BCK';
			$data['input_file_name']= $this->input_file_name;
			$this->load_view('bck/index_action', $data, true);
		}		
	}


	public function save(){
		$this->authCheckDetailAccess('c');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
            $check = $this->model->create($this->input->post(), $lampiran_insert);
           // $this->model->create($this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_verifikasi_bck_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_verifikasi_bck_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_verifikasi_bck_id");
			$data['title']	= 'Edit BCK';
			$data['input_file_name'] = $this->input_file_name;
			$this->load_view('bck/index_action', $data);
		}
		
	}

	public function update($int_verifikasi_bck_id){
		$this->authCheckDetailAccess('u');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				$check = $this->model->update($int_verifikasi_bck_id, $this->input->post(), $lampiran_insert);
			}else{
				$check = $this->model->update($int_verifikasi_bck_id, $this->input->post(), null);

			}
			
			$this->set_json(array_merge([ 'stat' => $check, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}

	public function confirm($int_verifikasi_bck_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_verifikasi_bck_id, false);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_verifikasi_bck_id/del");
			$data['title']	= 'Hapus Data KTP';
			$data['info']   = [ 'NIK' => $res->var_nik,
                               'Nama' => $res->var_nama,
                               'Tanggal Lahir' => $res->dt_tanggal_lahir];
			$this->load_view('bck/index_delete', $data);
		}
	}

	public function delete($int_verifikasi_bck_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_verifikasi_bck_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}

	public function confirm_del_lampiran($int_verifikasi_bck_img_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_verifikasi_bck_img_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("ver_bck/$int_verifikasi_bck_img_id/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('bck/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_verifikasi_bck_img_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_verifikasi_bck_img_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}	
}

>>>>>>> b4148bf50887d3bc96873404ee95799aa8a61f6a
