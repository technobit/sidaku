<<<<<<< HEAD
<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body" style="overflow:auto">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="kecamatan_filter" class="col-md-4 col-form-label">Kecamatan</label>
									<div class="col-md-8">
                                        <select id="int_kecamatan_id" name="int_kecamatan_id" class="form-control form-control-sm kecamatan_filter select2" style="width: 100%;" onchange="get_kelurahan(this,'int_kelurahan_id',<?=$kelurahan_usr?>)">
                                            <option value="<?=$kecamatan_usr?>">- Semua Kecamatan -</option>
                                            <?php 
                                                foreach($list_kecamatan as $kec){
                                                    echo '<option value="'.$kec->int_kecamatan_id.'">'.$kec->var_kecamatan.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
                                    <label for="kelurahan_filter" class="col-md-4 col-form-label">Kelurahan</label>
									<div class="col-md-8">
                                        <select id="int_kelurahan_id" name="int_kelurahan_id" class="form-control form-control-sm kelurahan_filter select2" style="width: 100%;">
                                            <option value="<?=$kelurahan_usr?>">- Semua Kelurahan - </option>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
                                    <label for="status_filter" class="col-md-4 col-form-label">Status</label>
									<div class="col-md-8">
                                        <select id="status_filter" name="status_filter" class="form-control form-control-sm status_filter select2" style="width: 100%;">
                                            <option value="">- Semua Status - </option>
                                            <option value="1">Sudah Verifikasi</option>
                                            <option value="2">Belum Verifikasi</option>
                                        </select>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <table class="table table-striped table-hover table-full-width" id="datatable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Tempat, Tanggal Lahir</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
=======
<div class="container-fluid">
    <div class="row">
        <section class="col-lg-12">
            <div class="card card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="<?=isset($breadcrumb->icon)? $breadcrumb->icon : 'far fa-circle'?>"></i>
                        <?=isset($breadcrumb->card_title)? $breadcrumb->card_title :  $breadcrumb->title?>
                    </h3>
                </div><!-- /.card-header -->
                <div class="card-body" style="overflow:auto">
                    <div id="filter" class="form-horizontal filter-date p-2 border-bottom">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
									<label for="kecamatan_filter" class="col-md-4 col-form-label">Kecamatan</label>
									<div class="col-md-8">
                                        <select id="int_kecamatan_id" name="int_kecamatan_id" class="form-control form-control-sm kecamatan_filter select2" style="width: 100%;" onchange="get_kelurahan(this,'int_kelurahan_id',<?=$kelurahan_usr?>)">
                                            <option value="<?=$kecamatan_usr?>">- Semua Kecamatan -</option>
                                            <?php 
                                                foreach($list_kecamatan as $kec){
                                                    echo '<option value="'.$kec->int_kecamatan_id.'">'.$kec->var_kecamatan.'</option>';
                                                }
                                            ?>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
                                    <label for="kelurahan_filter" class="col-md-4 col-form-label">Kelurahan</label>
									<div class="col-md-8">
                                        <select id="int_kelurahan_id" name="int_kelurahan_id" class="form-control form-control-sm kelurahan_filter select2" style="width: 100%;">
                                            <option value="<?=$kelurahan_usr?>">- Semua Kelurahan - </option>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group row text-sm mb-0">
                                    <label for="status_filter" class="col-md-4 col-form-label">Status</label>
									<div class="col-md-8">
                                        <select id="status_filter" name="status_filter" class="form-control form-control-sm status_filter select2" style="width: 100%;">
                                            <option value="">- Semua Status - </option>
                                            <option value="1">Sudah Verifikasi</option>
                                            <option value="2">Belum Verifikasi</option>
                                        </select>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <table class="table table-striped table-hover table-full-width" id="datatable">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>NIK</th>
                            <th>Nama</th>
                            <th>Tempat, Tanggal Lahir</th>
                            <th>Kecamatan</th>
                            <th>Kelurahan</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
<div id="ajax-modal" class="modal fade animate shake" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="75%" aria-hidden="true"></div>
<div id="ajax-modal-confirm" class="modal" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" data-width="50%" aria-hidden="true"></div>
>>>>>>> b4148bf50887d3bc96873404ee95799aa8a61f6a
