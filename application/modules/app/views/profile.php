<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="fas fa-user mr-1"></i>
                        Data Profile
                    </h3>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <?php
                            echo form_open($url_update, ['method' => "POST", "role" => "form", "class" => "form-horizontal", "id" =>"profile-form"]);
                            $msg = $this->session->flashdata('msg');
                            if(!empty($msg)){
                                echo '<div class="alert alert-'.(($this->session->flashdata('stat'))? 'success' : 'danger').'">'.$msg.'</div>';
                            }
                        ?>
                            <div class="form-group row mb-1">
                                <label for="group" class="col-sm-3 col-form-label">Group User</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="group" value="<?php echo $this->session->group?>" disabled />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Username</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="username" value="<?php echo $profile->username?>" disabled />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-3 col-form-label">Tanggal Register</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control form-control-sm" id="username" value="<?php echo $profile->tgl?>" disabled />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="inputName" class="col-sm-3 col-form-label">Nama Lengkap</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control form-control-sm" id="first_name" name="first_name" placeholder="Nama Depan" value="<?php echo $profile->first_name?>"/>
                                </div>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control form-control-sm" id="last_name" name="last_name" placeholder="Nama Depan" value="<?php echo $profile->last_name?>"/>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-success">Simpan Profil</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title mt-1">
                        <i class="fas fa-user mr-1"></i>
                        Data Access
                    </h3>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <?php
                            echo form_open($url_pass, ['method' => "POST", "role" => "form", "class" => "form-horizontal", "id" =>"password-form"]);
                            $p_msg = $this->session->flashdata('p_msg');
                            if(!empty($p_msg)){
                                echo '<div class="alert alert-'.(($this->session->flashdata('p_stat'))? 'success' : 'danger').'">'.$p_msg.'</div>';
                            }
                        ?>
                            <div class="form-group row mb-1">
                                <label for="group" class="col-sm-5 col-form-label">Password Lama</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control form-control-sm" id="old_password" name="old_password" />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-5 col-form-label">Password Baru</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control form-control-sm" id="new_password" name="new_password" />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="username" class="col-sm-5 col-form-label">Konfirmasi Password Baru</label>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control form-control-sm" id="conf_password" name="conf_password" />
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <div class="offset-sm-3 col-sm-9">
                                    <button type="submit" class="btn btn-danger">Ubah Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>