<script>
    $(document).ready(function(){
        $("#profile-form").validate({
            rules: {
                first_name: {
                    required: true,
                    maxlength: 20
                },
                last_name: {
                    required: true,
                    maxlength: 20
                }
            },
            validClass: "valid-feedback",
            errorElement: "div", // contain the error msg in a small tag
            errorClass: 'invalid-feedback',
            errorPlacement: erp,
            highlight: hl,
            unhighlight: uhl,
            success: sc
        });

        $("#password-form").validate({
            rules: {
                old_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                new_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                conf_password: {
                    required: true,
                    equalTo: "#new_password"
                }
            },
            validClass: "valid-feedback",
            errorElement: "div", // contain the error msg in a small tag
            errorClass: 'invalid-feedback',
            errorPlacement: erp,
            highlight: hl,
            unhighlight: uhl,
            success: sc
        });
    });
</script>