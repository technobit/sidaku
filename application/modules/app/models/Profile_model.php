<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Profile_model extends MY_Model {

	public function get_profile(){
		return $this->db->select("txt_nama_depan as first_name, txt_nama_belakang as last_name, txt_username as username, DATE_FORMAT(dt_registrasi, '%d %M %Y  %H:%I:%s') as tgl")
                        ->get_where($this->s_user, ['int_id_user' => $this->session->user_id])->row();
	}
	 
    public function update($in){
	    $this->db->where('int_id_user', $this->session->user_id)
                    ->update($this->s_user, ['txt_nama_depan' => $in['first_name'], 'txt_nama_belakang' => $in['last_name']]);
    }

    public function update_password($in){
        $data = $this->db->select("txt_password as password")
                    ->get_where($this->s_user, ['int_id_user' => $this->session->user_id])->row();

        if(password_verify($in['old_password'], $data->password)){
            $this->db->where('int_id_user', $this->session->user_id)
                ->update($this->s_user, ['txt_password' => password_hash($in['new_password'], PASSWORD_DEFAULT)]);

            return true;
        }
        return false;
    }
}