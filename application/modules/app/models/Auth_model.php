<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Auth_model extends MY_Model {
	private $userMenu = '';
	private $userCredential = array();
	private $userLoginMessage = '';
	private $userAccessMenu = array();
	private $userRedirect = '';

	public function getAuthCredential($username, $password){
		$return = false;
		$query = $this->db->query("SELECT  u.int_id_user as user_id, u.txt_nama_depan as first_name, u.txt_nama_belakang as last_name, u.txt_password as password, u.int_status as status, u.int_id_group as group_id, g.txt_nama as group_name, u.is_publish, u.int_kecamatan_id, u.int_kelurahan_id, u.var_rw, u.var_rt 
                                   FROM    {$this->s_user} u
                                   JOIN    {$this->s_group} g ON u.int_id_group = g.int_id_group 
                                   WHERE   u.txt_username = ?", [$username]);
		if($query->num_rows() == 1){
			$query = $query->row();

			if($query->status){
				if(password_verify($password, $query->password)){
					$this->userCredential = ['user_id' => $query->user_id,
											 'username' => $username,
											 'first_name' => $query->first_name,
											 'last_name' => $query->last_name,
                                             'group_id' => $query->group_id,
                                             'group' => $query->group_name,
                                             'int_kecamatan_id' => $query->int_kecamatan_id,
                                             'int_kelurahan_id' => $query->int_kelurahan_id,
                                             'var_rw' => $query->var_rw,
                                             'var_rt' => $query->var_rt,
                                             'publish' => $query->is_publish];
					
					$this->userAccessMenu['APP-HOME'] = ['c' => 0, 'r' => 1, 'u' => 0, 'd' => 0];
					$this->userAccessMenu['USER-PROFILE'] = ['c' => 0, 'r' => 1, 'u' => 1, 'd' => 0];
					$this->_getUserMenu($query->group_id);
					$this->userLoginMessage = 'Login berhasil. Silahkan tunggu...';
					$return = true;
				} else {
					$this->userLoginMessage = 'Username dan Password salah';
				}
			} else {
				$this->userLoginMessage = 'User tidak aktif.';
			}
		} else {
			$this->userLoginMessage = 'Username dan Password salah';
		} 
		return $return;
	}
	 
	private function _getUserMenu($group_id, $parent_id = null){
		$this->db->select("m.int_id_menu as menu_id, m.txt_kode as kode, m.txt_nama as nama, m.txt_url as url, m.int_level as level, m.txt_class as class, m.txt_icon as icon, (SELECT COUNT(*) FROM {$this->s_menu} mm WHERE mm.int_parent_id = m.int_id_menu) as sub, gm.c, gm.r, gm.u, gm.d")
							->from($this->s_group_menu.' gm')
							->join($this->s_menu.' m', 'gm.int_id_menu = m.int_id_menu')
							->where('gm.int_id_group', $group_id, false)
							->where('m.is_active', 1)
							->order_by('m.int_urutan');
		if(empty($parent_id)){
			$this->db->where("(m.int_parent_id is null or m.int_level = 1)");
		} else {
			$this->db->where("(m.int_parent_id = {$parent_id} and m.int_level > 1)", null, false);
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$data = $query->result();
			foreach ($data as $d) {
				$this->userAccessMenu[strtoupper($d->kode)] = ['c' => $d->c, 'r' => $d->r, 'u' => $d->u, 'd' => $d->d];
				if($d->sub == 0){
					$this->userMenu .=  '<li class="nav-item">' .
										'<a href="'.(empty($d->url)? '#' : site_url($d->url)).'" class="nav-link '.$d->class.' l'.$d->level.'">' .
										'<i class="nav-icon '.$d->icon.'"></i><p>'.$d->nama.'</p></a></li>';
				} else {
					$this->userMenu .= 	'<li class="nav-item has-treeview ">' .
                    					'<a href="#" class="nav-link '.$d->class.' l'.$d->level.'">' .
										'<i class="nav-icon '.$d->icon.'"></i>' .
										'<p>'.$d->nama.'<i class="fas fa-angle-left right"></i></p></a>' .
										'<ul class="nav nav-treeview">';

					$this->_getUserMenu($group_id, $d->menu_id);
					$this->userMenu .= '</ul>';
				}
				
				if(empty($this->userRedirect)){
					$this->userRedirect = site_url($d->url);
				}
			}
		}
	}

	public function getLoginMessage(){
		return $this->userLoginMessage;
	}

	public function getUserCredential(){
		return $this->userCredential;
	}

	public function getUserMenu(){
		return $this->userMenu;
	}

	public function getUserAccessMenu(){
		return $this->userAccessMenu;
	}
	
	public function getUserRedirect(){
		return $this->userRedirect;
	}
	
	public function	_getUserAccessMenu($group_id, $kode){
		return $this->db->query("	SELECT  c,r,u,d 
									FROM    {$this->s_group_menu} gm 
									JOIN    {$this->s_menu} m ON gm.menu_id = m.menu_id
									WHERE   gm.group_id = {$this->binaryUUID($group_id)} AND m.kode = ?", [$kode])->row();
	}
}