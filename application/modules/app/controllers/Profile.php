<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends MX_Controller {
	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'USER-PROFILE';
        $this->module   = 'app';
        $this->routeURL = 'profile';
		$this->authCheck();

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;

		$this->load->model('profile_model', 'model');
    }
	
	public function index(){
        $this->authCheckDetailAccess('r'); // hak akses untuk render page

        $this->breadcrumb->title = 'Profile';
        $this->breadcrumb->list = [getServerDate(true)];
		$this->js = true;
        $data['profile']        = $this->model->get_profile();
        $data['url_update']     = site_url("{$this->routeURL}");
        $data['url_pass']       = site_url("{$this->routeURL}/pass");
		$this->render_view('profile', $data, false);
	}

    public function update(){
        $this->authCheckDetailAccess('u'); // hak akses untuk render page

        $this->form_validation->set_rules('first_name', 'Nama Depan', 'required|min_length[3]|max_length[20]');
        $this->form_validation->set_rules('last_name', 'Nama Belakang', 'required|min_length[3]|max_length[20]');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('stat', false);
            $this->session->set_flashdata('msg', 'Data profile gagal disimpan. '.validation_errors('<br>', ' '));
        } else {
            $this->model->update($this->input_post());

            $this->session->set_userdata(['first_name' => $this->input_post('first_name'), 'last_name' => $this->input_post('last_name')]);

            $this->session->set_flashdata('stat', true);
            $this->session->set_flashdata('msg', 'Data profile berhasil disimpan.');
        }

        redirect('profile');
    }

    public function update_pass(){
        $this->authCheckDetailAccess('u'); // hak akses untuk render page

        $this->form_validation->set_rules('old_password', 'Password Lama', 'required|min_length[6]|max_length[20]');
        $this->form_validation->set_rules('new_password', 'Password Baru', 'trim|required|min_length[6]|max_length[20]');
        $this->form_validation->set_rules('conf_password', 'Konfirmasi Password', 'trim|required|matches[new_password]');

        if($this->form_validation->run() == FALSE){
            $this->session->set_flashdata('stat', false);
            $this->session->set_flashdata('msg', 'Password gagal diupdate. '.validation_errors('<br>', ' '));
        } else {
            $status = $this->model->update_password($this->input_post());
            if($status){
                $this->session->set_flashdata('p_stat', true);
                $this->session->set_flashdata('p_msg', 'Password berhasil di-update.');
            } else{
                $this->session->set_flashdata('p_stat', false);
                $this->session->set_flashdata('p_msg', 'Password gagal di-update. Password Lama salah.');
            }

        }

        redirect('profile');
    }
}
