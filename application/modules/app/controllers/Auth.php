<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Auth extends MX_Controller {
	
	function __construct()
    {
        parent::__construct();
        $this->load->model('app/auth_model', 'auth');

        $this->load->library('form_validation');
        $this->form_validation->CI =& $this;
    }
	
	public function login(){
        if(!$this->loginCheck()){
            $this->login_view();
        } else {
            redirect('/');
        }
    }
    
    public function do_login(){
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[20]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[4]|max_length[20]');
        
        if($this->form_validation->run() == FALSE){
            $this->set_json([  'stat' => false,
                                'msg' => 'Terjadi kesalahan', 
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
            if($this->auth->getAuthCredential($this->input->post('username', true), $this->input->post('password', true)) === true){
                $this->_setSessionUser();

                $this->set_json([  'stat' => true, 
                                    'msg' => $this->auth->getLoginMessage(), 
                                    'url' => $this->auth->getUserRedirect()
                                ]);
            } else {
                $this->set_json([  'stat' => false, 
                                    'msg' => $this->auth->getLoginMessage(), 
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                                'token' => $this->getCsrfToken()]
                                ]);
            }

        }
    }

    private function _setSessionUser(){
        $this->session->set_userdata('is_login', true);
        $this->session->set_userdata($this->auth->getUserCredential());
        $this->session->set_userdata('menu', $this->auth->getUserMenu());
        $this->session->set_userdata('access', $this->auth->getUserAccessMenu());
    }

    public function logout(){
        $this->session->set_userdata('is_login', false);
        $this->session->sess_destroy();

        redirect('login');
    }
}
