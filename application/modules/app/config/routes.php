<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['profile']['get']	= 'app/profile';
$route['profile']['post']	= 'app/profile/update';
$route['profile/pass']['post']	= 'app/profile/update_pass';