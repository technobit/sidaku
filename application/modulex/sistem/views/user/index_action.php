<?php
    $readOnly = isset($data->user_id)? (($data->user_id > 1)? '' : 'readonly') : '';
?>
<form method="post" action="<?=$url?>" role="form" class="form-horizontal" id="user-form" width="80%">
<div id="modal-user" class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel"><?=$title?></h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="form-message text-center"></div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">Group</label>
				<div class="col-sm-10">
					<select <?=$readOnly ?> id="group" name="group_id" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih -</option>
						<?php 
							foreach($group as $g){
								echo '<option value="'.$g->group_id.'">'.$g->nama.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="username" class="col-sm-2 col-form-label">Username</label>
				<div class="col-sm-10">
					<input type="text" class="form-control form-control-sm" id="username" placeholder="Username" name="username" value="<?=isset($data->username)? $data->username : ''?>"/>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="nama" class="col-sm-2 col-form-label">Nama</label>
				<div class="col-sm-5">
					<input type="text" class="form-control form-control-sm" id="first_name" placeholder="Nama Depan" name="first_name" value="<?=isset($data->first_name)? $data->first_name : ''?>"/>
				</div>
                <div class="col-sm-5">
                    <input type="text" class="form-control form-control-sm" id="last_name" placeholder="Nama Belakang" name="last_name" value="<?=isset($data->last_name)? $data->last_name : ''?>"/>
                </div>
			</div>
			<div class="form-group row mb-1">
				<label for="Password" class="col-sm-2 col-form-label">Passwords</label>
				<div class="col-sm-10">
					<input type="password" class="form-control form-control-sm" id="password" placeholder="Password" name="password" />
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">Wilayah</label>
				<div class="col-sm-10"></div>
			</div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">Kecamatan</label>
				<div class="col-sm-10">
					<select id="int_kecamatan_id" name="int_kecamatan_id" class="form-control form-control-sm select2" style="width: 100%;" onchange="get_kelurahan(this,'int_kelurahan_id',<?=isset($data->int_kelurahan_id)? $data->int_kelurahan_id : ''?>)">
						<option value="">- Pilih Kecamatan -</option>
						<?php 
							foreach($list_kecamatan as $kec){
								echo '<option value="'.$kec->int_kecamatan_id.'">'.$kec->var_kecamatan.'</option>';
							}
						?>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">Kelurahan</label>
				<div class="col-sm-10">
					<select id="int_kelurahan_id" name="int_kelurahan_id" class="form-control form-control-sm select2" style="width: 100%;" onchange="get_rw(this,'var_rw','<?=isset($data->var_rw)? $data->var_rw : ''?>')">
						<option value="">- Pilih Kelurahan - </option>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="group" class="col-sm-2 col-form-label">RW</label>
				<div class="col-sm-4">
					<select id="var_rw" name="var_rw" class="form-control form-control-sm select2" style="width: 100%;" onchange="get_rt(this,'var_rt','<?=isset($data->var_rt)? $data->var_rt : ''?>')">
						<option value="">- Pilih RW -</option>
					</select>
				</div>
				<label for="group" class="col-sm-2 col-form-label">RT</label>
				<div class="col-sm-4">
					<select id="var_rt" name="var_rt" class="form-control form-control-sm select2" style="width: 100%;">
						<option value="">- Pilih RT -</option>
					</select>
				</div>
			</div>
			<div class="form-group row mb-1">
				<label for="Status" class="col-sm-2 col-form-label">Status</label>
				<div class="col-sm-10 mt-1">
					<div class="icheck-primary d-inline mr-2">
						<input type="radio" id="radioPrimary1" name="is_aktif" value="1" <?=isset($data->is_aktif)? (($data->is_aktif == 1)? 'checked' : '') : 'checked' ?>>
							<label for="radioPrimary1">Aktif </label>
					</div>
					<div class="icheck-danger d-inline">
						<input type="radio" id="radioPrimary2" name="is_aktif" value="0" <?=isset($data->is_aktif)? (($data->is_aktif == 0)? 'checked' : '') : '' ?>>
						<label for="radioPrimary2">Non-aktif</label>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	function get_kelurahan(th,select_id,int_kelurahan_id){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("wilayah/kelurahan")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"), 
					int_kecamatan_id: $('#int_kecamatan_id').val()}
		}).done(function (data){
			refreshToken(data);
			opt = (data.data.length > 0)? '<option value="">- Pilih Kelurahan - </option>' : opt;
			$.each(data.data, function (i, item) {
				opt += '<option value="' + item.int_kelurahan_id + '">' + item.var_kelurahan + '</option>';
			});
			$('#'+select_id).html(opt).select2();
			if (int_kelurahan_id != ''){
				$('#'+select_id).val(int_kelurahan_id).trigger("change");
			}

		});
	}

	function get_rw(th,select_id,var_rw){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("wilayah/rw")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"),
					int_kecamatan_id: $('#int_kecamatan_id').val(),
					int_kelurahan_id: $(th).val()}
		}).done(function (data){
			refreshToken(data);
			opt = (data.data.length > 0)? '<option value="">- Pilih RW- </option>' : opt;
			$.each(data.data, function (i, item) {
				opt += '<option value="' + item.var_rw + '">' + item.var_rw + '</option>';
			});
			$('#'+select_id).html(opt).select2();
			if (var_rw != ''){
				$('#'+select_id).val(var_rw).trigger("change");
			}

		});
	}

	function get_rt(th,select_id,var_rt){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("wilayah/rt")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"),
					int_kecamatan_id: $('#int_kecamatan_id').val(),
					int_kelurahan_id: $('#int_kelurahan_id').val(),
					var_rw: $(th).val()}
		}).done(function (data){
			refreshToken(data);
			opt = (data.data.length > 0)? '<option value="">- Pilih RT- </option>' : opt;
			$.each(data.data, function (i, item) {
				opt += '<option value="' + item.var_rt + '">' + item.var_rt + '</option>';
			});
			$('#'+select_id).html(opt).select2();
			if (var_rt != ''){
				$('#'+select_id).val(var_rt).trigger("change");
			}

		});
	}

	$(document).ready(function(){
		$('.select2').select2();
		<?php if(isset($data->group_id)) echo '$("#group").val("'.$data->group_id.'").trigger("change");'?>
		<?php if(isset($data->int_kecamatan_id)) echo '$("#int_kecamatan_id").val("'.$data->int_kecamatan_id.'").trigger("change");'?>

		$("#user-form").validate({
			rules: {
				group_id: {
					required: true
				},
				username: {
					required: true,
					minlength: 3,
					maxlength: 20
				},
				first_name: {
					required: true,
					minlength: 4,
					maxlength: 50
				},
                last_name: {
                    minlength: 4,
                    maxlength: 50
                },
				password: {
					<?=isset($data)? '' : 'required: true,' ?>
					minlength: 4,
					maxlength: 20
				},
				is_aktif: {
					required: true
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
				blockUI(form);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
						unblockUI(form);
						setFormMessage('.form-message', data);
						if(data.stat){
							resetForm('#user-form');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>