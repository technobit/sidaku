<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action" width="80%">
<div id="modal-form" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<ul class="nav nav-tabs no-border-b" id="custom-content-below-tab" role="tablist" style="width:100%">
				<li class="nav-item">
					<a class="nav-link active" id="index-form-input-tab" data-toggle="pill" href="#index-form-input" role="tab" aria-controls="index-form-input" aria-selected="true"><?=$title?></a>
				</li>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="tab-pane fade show active" id="index-form-input" role="tabpanel" aria-labelledby="index-form-input-tab">
					<div class="form-group row mb-1" style="margin-bottom:20px !important">
						<label for="var_nik" class="col-sm-4 col-form-label">Nomer Induk Kependudukan (NIK/KTP)</label>
						<div class="col-sm-6">
							<input type="text" class="form-control form-control-sm" id="var_nikx" placeholder="Masukkan Nomer Induk Kependudukan (NIK/KTP)" name="var_nikx" value="<?=isset($data->var_nik)? $data->var_nik : ''?>" <?=isset($data->var_nik)? 'disabled' : ''?>/>
						</div>
						<div class="col-sm-2">
							<?php if(!isset($data->var_nik)){?>
								<button type="button" class="btn btn-sm btn-primary" id="btn_check_nik" onclick="check_nik()"><i class="fas fa-search"></i> Cari NIK</button>
							<?php } ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6" id="form_ktp_left" style="<?=isset($data->var_nik)? 'display:block' : 'display:none'?>">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Data Kependudukan</h3>
								</div>
								<div class="card-body">
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Nama</dd>
										<dt class="col-sm-8" id="var_namax"><?=isset($data->var_nama)? $data->var_nama : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Jenis Kelamin</dd>
										<dt class="col-sm-8" id="var_jeniskelamin"><?=isset($data->var_jeniskelamin)? $data->var_jeniskelamin : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Tempat, Tanggal Lahir</dd>
										<dt class="col-sm-8" id="var_tempat_tanggal_lahir">
											<?=isset($data->var_tempat_lahir)? $data->var_tempat_lahir : ''?>, 
											<?=isset($data->dt_tanggal_lahir)? $data->dt_tanggal_lahir : ''?>, 
										</dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Agama</dd>
										<dt class="col-sm-8" id="var_agama"><?=isset($data->var_agama)? $data->var_agama : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Status Perkawinan</dd>
										<dt class="col-sm-8" id="var_status_kawin"><?=isset($data->var_status_kawin)? $data->var_status_kawin : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Alamat</dd>
										<dt class="col-sm-8" id="var_alamat"><?=isset($data->var_alamat)? $data->var_alamat : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">RT/RW</dd>
										<dt class="col-sm-8" id="var_rt_rw">
											<?=isset($data->var_rt)? $data->var_rt : ''?>/
											<?=isset($data->var_rw)? $data->var_rw : ''?>
										</dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Kelurahan</dd>
										<dt class="col-sm-8" id="var_kelurahan"><?=isset($data->var_kelurahan)? $data->var_kelurahan : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Kecamatan</dd>
										<dt class="col-sm-8" id="var_kecamatan"><?=isset($data->var_kecamatan)? $data->var_kecamatan : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Nomor HP</dd>
										<div class="col-sm-8" >
											<input type="text" class="form-control form-control-sm" id="var_nohp" placeholder="No. Handphone" name="var_nohp" value="<?=isset($data->var_nohp)? $data->var_nohp : ''?>" />
										</div>
									</div>
									<input type="hidden" id="var_nik" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>"/>
									<input type="hidden" id="var_nama" name="var_nama" value="<?=isset($data->var_nama)? $data->var_nama : ''?>"/>
								</div>
							</div>
						</div>
						<div class="col-md-6" id="form_ktp_right" style="<?=isset($data->var_nik)? 'display:block' : 'display:none'?>">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Form Survey</h3>
								</div>
								<div class="card-body">
									<div class="form-group row mb-3" id="pertanyaan_1">
										<div class="col-sm-12">
											<dt>1. Apakah yang bersangkutan sudah memiliki KTP Elektronik?</dt>
										</div>
										<div class="col-sm-11 offset-sm-1 mt-2">
											<div class="icheck-primary d-inline int_jawaban_1">
												<input type="radio" id="int_jawaban_1a" name="int_jawaban_1" value="2" <?=isset($data->int_jawaban_1)? (($data->int_jawaban_1 == 2)? 'checked' : '') : ''?> onchange="jawaban_1()">
													<label for="int_jawaban_1a">Sudah </label>
											</div>
										</div>
										<div class="col-sm-11 offset-sm-1 mt-2">
											<div class="icheck-danger d-inline int_jawaban_1">
												<input type="radio" id="int_jawaban_1b" name="int_jawaban_1" value="1" <?=isset($data->int_jawaban_1)? (($data->int_jawaban_1 == 1)? 'checked' : '') : '' ?> onchange="jawaban_1()">
												<label for="int_jawaban_1b">Belum</label>
											</div>
										</div>
									</div>
									<div class="form-group mb-1">
										<label id="form_notes"></label>
									</div>
									<div class="form-group mb-1" id="in_keterangan">
										<label for="var_keterangan">Keterangan</label>
										<div>
											<input type="text" class="form-control form-control-sm" id="var_keterangan" placeholder="Keterangan" name="var_keterangan" value="<?=isset($data->var_keterangan)? $data->var_keterangan : ''?>" />
										</div>
									</div>
									<div class="form-group row mb-1 mt-3">
										<div class="col-sm-4">
											<div class="icheck-primary d-inline is_verified">
												<input type="radio" id="radio_btn1" name="is_verified" value="1" <?=isset($data->is_verified)? (($data->is_verified == 1)? 'checked' : '') : ''?>>
													<label for="radio_btn1">Valid </label>
											</div>
										</div>
										<div class="col-sm-8">
											<div class="icheck-danger d-inline is_verified">
												<input type="radio" id="radio_btn2" name="is_verified" value="0" <?=isset($data->is_verified)? (($data->is_verified == 0)? 'checked' : '') : '' ?>>
												<label for="radio_btn2">Belum Valid</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success" id="save_button" style="<?=isset($data->var_nik)? '' : 'display:none'?>">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	function check_nik(){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("nik/cek")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"), var_nik: $('#var_nikx').val()}
		}).done(function (data){
			refreshToken(data);
			setFormMessage('.form-message', data, 5000);
			if(data.data.length > 0){
				document.getElementById('form_ktp_left').style.display = 'block';
				document.getElementById('form_ktp_right').style.display = 'block';
				document.getElementById('save_button').style.display = 'block';
				arrdata = data.data[0];
				document.getElementById("var_namax").innerHTML = arrdata.var_nama;
				document.getElementById("var_jeniskelamin").innerHTML = arrdata.var_jeniskelamin;
				document.getElementById("var_tempat_tanggal_lahir").innerHTML = arrdata.var_tempat_lahir+', '+arrdata.dt_tanggal_lahir;
				document.getElementById("var_agama").innerHTML = arrdata.var_agama;
				document.getElementById("var_status_kawin").innerHTML = arrdata.var_status_kawin;
				document.getElementById("var_alamat").innerHTML = arrdata.var_alamat;
				document.getElementById("var_kecamatan").innerHTML = arrdata.var_kecamatan;
				document.getElementById("var_kelurahan").innerHTML = arrdata.var_kelurahan;
				document.getElementById("var_rt_rw").innerHTML = arrdata.var_rt+'/'+arrdata.var_rw;
				document.getElementById('var_nikx').disabled = true;
				document.getElementById('btn_check_nik').disabled = true;
				$('#var_nik').val(arrdata.var_nik);
				$('#var_nama').val(arrdata.var_nama);
			}
		});
	}

	$(document).ready(function(){
		$("#action").validate({
			rules: {
			    var_nikx:{
					required: true,
					digits: true,
					minlength: 16,
					maxlength: 16
				},
			    var_nik:{
					required: true,
					digits: true,
					minlength: 16,
					maxlength: 16
				},
			    var_nohp:{
			        required: true,
					digits: true,
					minlength: 10,
					maxlength: 13
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#action');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});
	});
</script>