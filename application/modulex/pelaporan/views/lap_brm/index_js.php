<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script>
    var dataTable;
    $(document).ready(function() {
        dataTable = $('#datatable').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ],
            "aoColumnDefs": [{
                    "aTargets": [4],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '';
                                break;
                            case 1:
                                return '<span class="badge bg-danger">Belum</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-success">Sudah</span>';
                                break;
                            default: '';
                        }
                    }
                },
                {
                    "aTargets": [6],
                    "mRender": function(data, type, row, meta) {
                        switch (data) {
                            case 0:
                                return '';
                                break;
                            case 1:
                                return '<span class="badge bg-danger">Belum</span>';
                                break;
                            case 2:
                                return '<span class="badge bg-success">Sudah</span>';
                                break;
                            default: '';
                        }
                    }
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
    });
</script>