<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Lap_kba_model extends MY_Model {

    public function list($filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("*, CONCAT(suc.txt_nama_depan, ' ', suc.txt_nama_belakang) as created,
						CONCAT(suu.txt_nama_depan, ' ', suu.txt_nama_belakang) as updated")
					->from($this->t_verifikasi_kba." tvb")
					->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik", "left")
					->join($this->s_user." suc", "tvb.created_by = suc.int_id_user", "left")
					->join($this->s_user." suu", "tvb.updated_by = suu.int_id_user", "left");;

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('tvb.var_nik', $filter)
					->or_like('var_nama', $filter)
					->group_end();
		}

		$order = 'tvb.var_nik ';
		switch($order_by){
			case 1 : $order = 'tvb.var_nik '; break;
			case 2 : $order = 'tvb.var_nama '; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($filter = NULL){
		$this->db->from($this->t_verifikasi_kba);

        if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('var_nik', $filter)
			->or_like('var_nama', $filter)
                ->group_end();
        }
		return $this->db->count_all_results();
	}

	public function create($ins, $lampiran = []){
		$var_jenis_img = $ins['var_jenis_img'];
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$ins = $this->clearFormInsert($ins, ['lampiran', 'var_jenis_img', 'sidaku_token', 'cdImg']);

		$this->db->insert($this->t_verifikasi_kba, $ins);
		$int_verifikasi_kba_id = $this->db->insert_id();
		$this->setQueryLog('ins_posts');

		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_verifikasi_kba_id' => $int_verifikasi_kba_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_verifikasi_kba_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

	public function get($int_verifikasi_kba_id, $with_lampiran = true){
		$data =  $this->db->select("*")
						->from($this->t_verifikasi_kba." tvb")
						->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik", "left")
						->join($this->m_jeniskelamin." mj", "mvk.int_jeniskelamin_id = mj.int_jeniskelamin_id", "left")
						->join($this->m_status_kawin." msk", "mvk.int_status_kawin_id = msk.int_status_kawin_id", "left")
						->join($this->m_kecamatan." mkc", "mvk.int_kecamatan_id = mkc.int_kecamatan_id", "left")
						->join($this->m_kelurahan." mkl", "mvk.int_kelurahan_id = mkl.int_kelurahan_id", "left")
						->where('tvb.int_verifikasi_kba_id', $int_verifikasi_kba_id)->get()->row_array();

		if(!empty($data)){
			$lampiran = ($with_lampiran)? $this->get_lampiran($int_verifikasi_kba_id) : null;
		}

		return (object) array_merge($data, ['lampiran' => $lampiran]);
	}
				
	public function get_lampiran($int_verifikasi_kba_id){
		return $this->db->query("SELECT *
								FROM  {$this->t_verifikasi_kba_img} 
								WHERE int_verifikasi_kba_id = {$int_verifikasi_kba_id}")->result();
			
	}

	public function update($int_verifikasi_kba_id, $upd, $lampiran = []){
		$var_jenis_img = $upd['var_jenis_img'];
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$upd = $this->clearFormInsert($upd, ['lampiran', 'var_jenis_img', 'sidaku_token', 'cdImg']);

		$this->db->where('int_verifikasi_kba_id', $int_verifikasi_kba_id);
		$this->db->update($this->t_verifikasi_kba, $upd);
		$this->setQueryLog('upd_kba');
		
		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_verifikasi_kba_id' => $int_verifikasi_kba_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_verifikasi_kba_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_verifikasi_kba_id){
		$ins['deleted_at'] = date("Y-m-d H:i:s");
		$ins['deleted_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$this->db->delete($this->t_verifikasi_kba,  ['int_verifikasi_kba_id' => $int_verifikasi_kba_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get_lampiran_img($int_verifikasi_kba_img_id){
		return $this->db->query("SELECT *
								FROM 	{$this->t_verifikasi_kba_img} 
								WHERE 	int_verifikasi_kba_img_id = {$int_verifikasi_kba_img_id}")->row();
	}
	
	public function delete_lampiran_img($int_verifikasi_kba_img_id){
		$data = $this->get_lampiran_img($int_verifikasi_kba_img_id);
		if(!empty($data)){
			//$upload_path= $this->config->item('upload_path');
			//$img_dir = $this->config->item('img_dir');
			//$unlink_dir = str_replace($img_dir,"",$upload_path);
			unlink(FCPATH.'/'.$data->var_image);
			
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->t_verifikasi_kba_img} WHERE int_verifikasi_kba_img_id = {$int_verifikasi_kba_img_id}");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}


}
