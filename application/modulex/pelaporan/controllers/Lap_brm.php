<?php
defined('BASEPATH') OR exit('No direct script access allowed');
	
class Lap_brm extends MX_Controller {
	private $input_file_name = 'lampiran';

	function __construct(){
		parent::__construct();
		
		$this->kodeMenu = 'LAP-BRM'; // kode data pada tabel data, 1 data : 1 controller
		$this->module   = 'pelaporan';
		$this->routeURL = 'lap_brm';
		$this->authCheck();
		
		$this->load->library('form_validation');
        $this->form_validation->CI =& $this;
		
		$this->load->model('lap_brm_model', 'model');

    }
	
	public function index(){
		$this->authCheckDetailAccess('r'); // hak akses untuk render page

		$this->page->subtitle = 'Pelaporan Belum Rekam KTP';
		$this->page->menu 	  = 'pelaporan';
		$this->page->submenu1 = 'lap_verifikasi';
		$this->page->submenu2 = 'lap_brm';
		$this->breadcrumb->title = 'Pelaporan Verifikasi Data KTP';
		$this->breadcrumb->card_title = 'Belum Rekam KTP';
		$this->breadcrumb->icon = 'fas fa-camera-retro';
		$this->breadcrumb->list = ['Pelaporan', 'Verifikasi Data KTP', 'Belum Rekam'];
		$this->css = true;
		$this->js = true;
		$data['url'] = site_url("{$this->routeURL}/add");
		$this->render_view('lap_brm/index', $data, true);
	}

	public function list(){
		$this->authCheckDetailAccess('r'); 

		$data  = array();
		$total = $this->model->listCount($this->input_post('search[value]', TRUE));
		$ldata = $this->model->list($this->input_post('search[value]', TRUE), $this->input_post('order[0][column]', true), $this->input_post('order[0][dir]'), $this->input_post('length', true), $this->input_post('start', true));

		$i 	   = $this->input_post('start', true);
		foreach($ldata as $d){
			$i++;
			if($d->int_jawaban_2 == 0){
				$lokasi = $d->var_lokasi_perekaman;
			}else if($d->int_jawaban_2 == 1){
				$lokasi = 'Kota Blitar';
			}else if($d->int_jawaban_2 == 2){
				$lokasi = 'Luar Kota';
			}
			$data[] = array($i, $d->var_nik, $d->var_nama, $d->var_nohp, $d->int_jawaban_1, $lokasi, $d->int_jawaban_3, $d->created, $d->updated);
		}
		$this->set_json(array( 'stat' => TRUE,
								'iTotalRecords' => $total,
								'iTotalDisplayRecords' => $total,
								'aaData' => $data,
								$this->getCsrfName() => $this->getCsrfToken()));
	}

	public function add(){
		if($this->authCheckDetailAccess('c', true) == false) return; // hak akses untuk modal popup
		$data['url']        = site_url("{$this->routeURL}/save");
		$data['title']      = 'Form BRM';
		$data['input_file_name'] = $this->input_file_name;
		$this->load_view('brm/index_action', $data, true);
		
	}

	public function save(){
		$this->authCheckDetailAccess('c');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
        
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'mc' => false, //modal close
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        } else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
			}
			
            $check = $this->model->create($this->input->post(), $lampiran_insert);
           // $this->model->create($this->input_post());
			$this->set_json([  'stat' => $check, 
								'mc' => $check, //modal close
								'msg' => "Data Saved Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							]);

        }
	}
	
	public function get($int_verifikasi_brm_id){
		if($this->authCheckDetailAccess('u', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_verifikasi_brm_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Error', 'title' => 'Data Not Found', 'message' => '']],true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("{$this->routeURL}/$int_verifikasi_brm_id");
			$data['title']	= 'Edit BRM';
			$data['input_file_name'] = $this->input_file_name;
			$this->load_view('brm/index_action', $data);
		}
		
	}

	public function update($int_verifikasi_brm_id){
		$this->authCheckDetailAccess('u');

		$year = date("Y");
		$month = date("m");
		$img_dir = $this->config->item('img_dir').$year."/".$month; 
		$upload_path = $this->config->item('upload_path').$year."/".$month; 
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777, true);
		}
		$this->form_validation->set_rules('var_nik', 'NIK', 'numeric|min_length[16]|max_length[16]');
		
        if($this->form_validation->run() == FALSE){
			$this->set_json([  'stat' => false,
								'msg' => "Data Validation Failed",
                                'msgField' => $this->form_validation->error_array(), 
                                'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
                            ]);
        }  else {
			$lampiran_insert  = [];
			$lampiran_msg 	= [];
			$countFiles		= 0;
			$files 			= [];
			if(isset($_FILES[$this->input_file_name])){		
				$config['upload_path']   = $upload_path;
				$config['allowed_types'] = $this->config->item('allowed_types'); 
				$config['encrypt_name']  = $this->config->item('encrypt_name');
				$config['max_size']      = $this->config->item('max_size');

				$this->load->library('upload', $config);
				
				$countFiles = count($_FILES[$this->input_file_name]['name']);
				$cdImg		= $this->input->post('cdImg');
				
				for ($i = 0; $i < $countFiles ; $i++) { 
					if(!empty($_FILES[$this->input_file_name]['name'][$i])){
						$_FILES['gambar']['name']     = $_FILES[$this->input_file_name]['name'][$i];
						$_FILES['gambar']['type']     = $_FILES[$this->input_file_name]['type'][$i];
						$_FILES['gambar']['tmp_name'] = $_FILES[$this->input_file_name]['tmp_name'][$i];
						$_FILES['gambar']['error']    = $_FILES[$this->input_file_name]['error'][$i];
						$_FILES['gambar']['size']     = $_FILES[$this->input_file_name]['size'][$i];
						
						if($this->upload->do_upload('gambar')){
							$lampiran_insert[] 	= array_merge($this->upload->data(), 
																['dir' => $img_dir]);
							$lampiran_msg[$i] = ['stat' => true, 'msg' => 'Upload Success', 'cdImg' => $cdImg[$i]];
						}else{
							$lampiran_msg[$i] = ['stat' => false, 'msg' => $this->upload->display_errors('',''), 'cdImg' => $cdImg[$i]];  
						}
						$this->upload->error_msg = [];
					}
				}
				$files = ['files' => $lampiran_msg];
				$check = $this->model->update($int_verifikasi_brm_id, $this->input->post(), $lampiran_insert);
			}else{
				$check = $this->model->update($int_verifikasi_brm_id, $this->input->post(), null);

			}
			
			$this->set_json(array_merge([ 'stat' => $check, 
								'mc' => true,
								'msg' => "Data Updated Successfully",
								'csrf' => [ 'name' => $this->getCsrfName(),
                                            'token' => $this->getCsrfToken()]
							], $files));
		}
	}

	public function confirm($int_mverifikasi_ktp_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get($int_mverifikasi_ktp_id, false);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Error Detected.', 'message' => 'Data not found. ']], true);
		}else{
			$data['url']	= site_url("{$this->routeURL}/$int_mverifikasi_ktp_id/del");
			$data['title']	= 'Hapus Data KTP';
			$data['info']   = [ 'NIK' => $res->var_nik,
                               'Nama' => $res->var_nama,
                               'Tanggal Lahir' => $res->dt_tanggal_lahir];
			$this->load_view('brm/index_delete', $data);
		}
	}

	public function delete($int_mverifikasi_ktp_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete($int_mverifikasi_ktp_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}

	public function confirm_del_lampiran($int_verifikasi_brm_img_id){
		if($this->authCheckDetailAccess('d', true) == false) return; // hak akses untuk modal popup

		$res = $this->model->get_lampiran_img($int_verifikasi_brm_img_id);
		if(empty($res)){
			$this->modal_error(['data' => (object) ['header' => 'Data error', 'title' => 'Image Not Found.', 'message' => '']], true);
		}else{
			$data['data'] 	= $res;
			$data['url']	= site_url("ver_brm/$int_verifikasi_brm_img_id/lampiran_del");
			$data['title']	= 'Delete Image';
			$this->load_view('brm/index_delete_lampiran', $data);
		}
	}

	public function delete_lampiran($int_verifikasi_brm_img_id){
		$this->authCheckDetailAccess('d');

		$check = $this->model->delete_lampiran_img($int_verifikasi_brm_img_id);
		$this->set_json([  'stat' => $check, 
							'mc' => $check, //modal close
							'msg' => ($check)? "Data Deleted Successfully" : "Data Delete Failed",
							'csrf' => [ 'name' => $this->getCsrfName(),
										'token' => $this->getCsrfToken()]
						]);
	}	
}
