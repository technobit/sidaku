<form method="post" action="<?=$url ?>" role="form" class="form-horizontal" id="action" width="80%">
<div id="modal-form" class="modal-dialog modal-xl" role="document">
	<div class="modal-content">
		<div class="modal-header pb-0">
			<ul class="nav nav-tabs no-border-b" id="custom-content-below-tab" role="tablist" style="width:100%">
				<li class="nav-item">
					<a class="nav-link active" id="index-form-input-tab" data-toggle="pill" href="#index-form-input" role="tab" aria-controls="index-form-input" aria-selected="true"><?=$title?></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="index-form-lampiran-tab" data-toggle="pill" href="#index-form-lampiran" role="tab" aria-controls="index-form-lampiran" aria-selected="false">Lampiran</a>
				</li>
			</ul>	
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			<div class="tab-content pt-1" id="custom-content-below-tabContent">
				<div class="form-message text-center"></div>
				<div class="tab-pane fade show active" id="index-form-input" role="tabpanel" aria-labelledby="index-form-input-tab">
					<div class="row">
						<div class="col-md-6" id="form_ktp_left" style="<?=isset($data->var_nik)? 'display:block' : 'display:none'?>">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Data Kependudukan</h3>
								</div>
								<div class="card-body">
									<div class="form-group row mb-1">
										<dd class="col-sm-4">NIK</dd>
										<dt class="col-sm-8" id="var_nikx"><?=isset($data->var_nik)? $data->var_nik : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Nama</dd>
										<dt class="col-sm-8" id="var_namax"><?=isset($data->var_nama)? $data->var_nama : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Jenis Kelamin</dd>
										<dt class="col-sm-8" id="var_jeniskelamin"><?=isset($data->var_jeniskelamin)? $data->var_jeniskelamin : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Tempat, Tanggal Lahir</dd>
										<dt class="col-sm-8" id="var_tempat_tanggal_lahir">
											<?=isset($data->var_tempat_lahir)? $data->var_tempat_lahir : ''?>, 
											<?=isset($data->dt_tanggal_lahir)? $data->dt_tanggal_lahir : ''?>, 
										</dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Agama</dd>
										<dt class="col-sm-8" id="var_agama"><?=isset($data->var_agama)? $data->var_agama : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Status Perkawinan</dd>
										<dt class="col-sm-8" id="var_status_kawin"><?=isset($data->var_status_kawin)? $data->var_status_kawin : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Alamat</dd>
										<dt class="col-sm-8" id="var_alamat"><?=isset($data->var_alamat)? $data->var_alamat : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">RT/RW</dd>
										<dt class="col-sm-8" id="var_rt_rw">
											<?=isset($data->var_rt)? $data->var_rt : ''?>/
											<?=isset($data->var_rw)? $data->var_rw : ''?>
										</dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Kelurahan</dd>
										<dt class="col-sm-8" id="var_kelurahan"><?=isset($data->var_kelurahan)? $data->var_kelurahan : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Kecamatan</dd>
										<dt class="col-sm-8" id="var_kecamatan"><?=isset($data->var_kecamatan)? $data->var_kecamatan : ''?></dt>
									</div>
									<div class="form-group row mb-1">
										<dd class="col-sm-4">Nomor HP</dd>
										<div class="col-sm-8" >
											<input type="text" class="form-control form-control-sm" id="var_nohp" placeholder="No. Handphone" name="var_nohp" value="<?=isset($data->var_nohp)? $data->var_nohp : ''?>" />
										</div>
									</div>
									<input type="hidden" id="var_nik" name="var_nik" value="<?=isset($data->var_nik)? $data->var_nik : ''?>"/>
									<input type="hidden" id="var_nama" name="var_nama" value="<?=isset($data->var_nama)? $data->var_nama : ''?>"/>
									<input type="hidden" id="var_jenis_img" name="var_jenis_img" value="<?=isset($data->var_jenis_img)? $data->var_jenis_img : '-'?>"/>
								</div>
							</div>
						</div>
						<div class="col-md-6" id="form_ktp_right" style="<?=isset($data->var_nik)? 'display:block' : 'display:none'?>">
							<div class="card">
								<div class="card-header">
									<h3 class="card-title">Form Survey</h3>
								</div>
								<div class="card-body">
									<div class="form-group row mb-3" id="pertanyaan_1">
										<div class="col-sm-12">
											<dt>1. Apakah yang bersangkutan sudah memiliki KTP Elektronik?</dt>
										</div>
										<div class="col-sm-11 offset-sm-1 mt-2">
											<div class="icheck-primary d-inline int_jawaban_1">
												<input type="radio" id="int_jawaban_1a" name="int_jawaban_1" value="2" <?=isset($data->int_jawaban_1)? (($data->int_jawaban_1 == 2)? 'checked' : '') : ''?> onchange="jawaban_1()">
													<label for="int_jawaban_1a">Sudah </label>
											</div>
										</div>
										<div class="col-sm-11 offset-sm-1 mt-2">
											<div class="icheck-danger d-inline int_jawaban_1">
												<input type="radio" id="int_jawaban_1b" name="int_jawaban_1" value="1" <?=isset($data->int_jawaban_1)? (($data->int_jawaban_1 == 1)? 'checked' : '') : '' ?> onchange="jawaban_1()">
												<label for="int_jawaban_1b">Belum</label>
											</div>
										</div>
									</div>
									<div class="form-group mb-1">
										<label id="form_notes"></label>
									</div>
									<div class="form-group mb-1" id="in_keterangan">
										<label for="var_keterangan">Keterangan</label>
										<div>
											<input type="text" class="form-control form-control-sm" id="var_keterangan" placeholder="Keterangan" name="var_keterangan" value="<?=isset($data->var_keterangan)? $data->var_keterangan : ''?>" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="index-form-lampiran" role="tabpanel" aria-labelledby="index-form-lampiran-tab">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group row mb-1">
								<div class="col-md-12">
									<div class="custom-file">
										<input type="file" class="custom-file-input" name="<?=$input_file_name?>[]" id="customFile" accept="image/*" multiple>
										<label class="custom-file-label" for="customFile">Choose file</label>
									</div>
								</div>
							</div>
							<div class="row preview-images-zone">
							<?php 
								if(isset($data->lampiran)):
									foreach($data->lampiran as $lmp):
							?>
								<div class=" col-sm-4 img-server img-<?=$lmp->int_verifikasi_bck_img_id?>">
									<div class="preview-image preview-show-<?=$lmp->int_verifikasi_bck_img_id?> img-popup">
										<div class="image-zone" data-responsive="<?=cdn_url().$lmp->var_image ?> 375, <?=cdn_url().$lmp->var_image ?> 480, <?=cdn_url().$lmp->var_image ?> 800" data-src="<?=cdn_url().$lmp->var_image ?>" data-sub-html="<?=$lmp->var_jenis_img?>">
											<a href="<?=cdn_url().$lmp->var_image ?>">
												<img id="pro-img-<?=$lmp->int_verifikasi_bck_img_id?>" class="img-thumb-sm" src="<?=cdn_url().$lmp->var_image ?>">
											</a>
										</div>
									</div>
									<a href="#" class="btn btn-sm btn-danger ajax_modal_confirm" data-url="<?=site_url('ver_bck/'.$lmp->int_verifikasi_bck_img_id.'/lampiran')?>" data-block="#modal-form" style="display: block;width: 260px;margin-top: 0;margin-bottom: 10px;margin-left: auto;margin-right: auto;"><i class="fa fa-trash"></i> </a> 
								</div>
							
							<?php endforeach;
								endif;
							?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="modal-footer">
			<button type="button" data-dismiss="modal" class="btn btn-danger">Keluar</button>
			<button type="submit" class="btn btn-success" id="save_button" style="<?=isset($data->var_nik)? '' : 'display:none'?>">Simpan</button>
		</div>
	</div>
</div>
<?=form_close() ?>

<script>
	function jawaban_1(){
		var jawaban_1 = document.getElementsByName('int_jawaban_1');
		for (var i = 0, length = jawaban_1.length; i < length; i++) {
			if (jawaban_1[i].checked) {
				if(jawaban_1[i].value == 1){
					document.getElementById("form_notes").innerHTML = '';
				}else if (jawaban_1[i].value == 2){
					document.getElementById("form_notes").innerHTML = '<i class="required">*Silahkan Lampirkan Foto KTP</i>';
					$('#var_jenis_img').val('ktp');
				}else{

				}
			}
		}
	}

	$(document).ready(function(){
		jawaban_1();
		$(".img-popup").lightGallery();
		$("#action").validate({
			rules: {
			    var_nikx:{
					required: true,
					digits: true,
					minlength: 16,
					maxlength: 16
				},
			    var_nik:{
					required: true,
					digits: true,
					minlength: 16,
					maxlength: 16
				},
			    var_nohp:{
			        required: true,
					digits: true,
					minlength: 10,
					maxlength: 13
				}
			},
			submitHandler: function(form) {
				$('.form-message').html('');
                let blc = '#modal-form';
                blockUI(blc);
				$(form).ajaxSubmit({
					dataType:  'json',
					data: {<?=$page->tokenName ?> : $('meta[name=<?=$page->tokenName ?>]').attr("content")},
					success: function(data){
                        unblockUI(blc);
                        setFormMessage('.form-message', data);
                        if(data.stat){
							resetForm('#action');
							dataTable.draw();
						}
						closeModal($modal, data);
					}
				});
			},
			validClass: "valid-feedback",
			errorElement: "div", // contain the error msg in a small tag
			errorClass: 'invalid-feedback',
			errorPlacement: erp,
			highlight: hl,
			unhighlight: uhl,
			success: sc
		});

		bsCustomFileInput.init();
		$( ".preview-images-zone" ).sortable();
		$(document).on('click', '.image-cancel', function() {
			let no = $(this).data('no');
			//$(".preview-image.preview-show-"+no).parent().parent().remove();
			$(this).parent().parent().remove();
		});
		
		var num = 0;
		$('#customFile').change(function(){
			if (window.File && window.FileList && window.FileReader) {
			$('.preview-images-zone').find('div.img-client').remove();
			var files = event.target.files; //FileList object
			for (let i = 0; i < files.length; i++) {
				var file = files[i];
				if (!file.type.match('image')) continue;
				
				var picReader = new FileReader();
				let cdImg 	= 'img'+getSandStr(3,'-',3);
				let imgSize = (file.size/1000).toFixed(2);
				let imgType = file.type;
				
				picReader.addEventListener('load', function (event) {
					var picFile = event.target;
					var html =  '<div class="col-sm-4 img-client"><td class="align-top">' +
								'<div class="preview-image preview-show-' + i + '">' +
								'<div class="image-zone"><img id="pro-img-' + i + '" src="' + picFile.result + '"></div>' +
								'<input type="hidden" name="cdImg['+i+']" value="'+cdImg+'"/></div></div>';
					$(html).appendTo(".preview-images-zone");
				});

				//$( ".preview-images-zone" ).sortable();
				picReader.readAsDataURL(file);
			}
				$("#pro-image").val('');
			}else {
				alert('Browser not support');
			}
		});	});
</script>