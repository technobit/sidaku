<script src="<?=base_url()?>assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/dist/js/lightgallery-all.min.js"></script>
<script src="<?=base_url() ?>assets/plugins/lightGallery/lib/jquery.mousewheel.min.js"></script>
<script>
	function get_kelurahan(th,select_id,int_kelurahan_id){
		$.ajax({
			type: 'POST',
			url: '<?=site_url("wilayah/kelurahan_usr")?>',
			dataType: 'json',
			data: {<?=$page->tokenName?>: $('meta[name=<?=$page->tokenName?>]').attr("content"), 
					int_kecamatan_id: $('#int_kecamatan_id').val()}
		}).done(function (data){
			refreshToken(data);
			opt = (data.data.length > 0)? '<option value="<?=$kelurahan_usr?>">- Semua Kelurahan - </option>' : opt;
			$.each(data.data, function (i, item) {
				opt += '<option value="' + item.int_kelurahan_id + '">' + item.var_kelurahan + '</option>';
			});
			$('#'+select_id).html(opt).select2();
			if (int_kelurahan_id != '0'){
				$('#'+select_id).val(int_kelurahan_id).trigger("change");
			}

		});
	}

    var dataTable;
    $(document).ready(function() {
		$('.select2').select2();
        dataTable = $('#datatable').DataTable({
            "bFilter": true,
            "bServerSide": true,
            "bAutoWidth": false,
            "bProcessing": true,
            "pageLength": 50,
            "lengthMenu": [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]],
            "ajax": {
                "url": "<?=site_url("{$routeURL}") ?>",
                "dataType": "json",
                "type": "POST",
                "data": function(d) {
                    d.<?=$page->tokenName ?> = $('meta[name=<?=$page->tokenName ?>]').attr("content");
					d.kecamatan_filter = $('.kecamatan_filter').val();
					d.kelurahan_filter = $('.kelurahan_filter').val();
					d.status_filter = $('.status_filter').val();
                },
                "dataSrc": function(json) {
                    if (json.<?=$page->tokenName ?> !== undefined) $('meta[name=<?=$page->tokenName ?>]').attr("content", json.<?=$page->tokenName ?>);
                    return json.aaData;
                }
            },
            "aoColumns": [{
                    "sWidth": "10",
                    "class": "text-right",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto"
                },
                {
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                },
                {
                    "sWidth": "auto",
                },
                {
                    "sWidth": "auto",
                },
				{
                    "sWidth": "auto",
                    "bSearchable": false,
                    "bSortable": false
                }
            ]
        });
        $('.dataTables_filter input').unbind().bind('keyup', function(e) {
			if (e.keyCode == 13) {
				dataTable.search($(this).val()).draw();
			}
        });
        
        $('.kecamatan_filter').change(function(){
            dataTable.draw();
        });

        $('.kelurahan_filter').change(function(){
            dataTable.draw();
        });

        $('.status_filter').change(function(){
            dataTable.draw();
        });

    });
</script>