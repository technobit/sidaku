<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Ver_brm_model extends MY_Model {

	public function get_lokasi_perekaman(){
		$this->db->select("*")
		->from($this->m_lokasi_perekaman);

		return $this->db->order_by('int_lokasi_perekaman_id', 'ASC')->get()->result();
	}

    public function list($kecamatan_filter = 0, $kelurahan_filter = 0, $status_filter = "", $filter = NULL, $order_by = 0, $sort = 'ASC', $limit = 0, $ofset = 0){
		$this->db->select("mvk.*, tvb.int_verifikasi_brm_id, mk.var_kecamatan, ml.var_kelurahan")
					->from($this->m_verifikasi_ktp." mvk")
					->join($this->t_verifikasi_brm." tvb", "mvk.var_nik = tvb.var_nik", "left")
					->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left")
					->where('mvk.int_kategori_ktp_id', 1);

		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mvk.int_kecamatan_id', $kecamatan_filter);
		}

		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mvk.int_kelurahan_id', $kelurahan_filter);
		}

		if(($status_filter == 1)){ // filter
			$this->db->where('tvb.int_verifikasi_brm_id != ""');
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
			$this->db->group_start()
					->like('mvk.var_nik', $filter)
					->or_like('mvk.var_nama', $filter)
					->or_like('mk.var_kecamatan', $filter)
					->or_like('ml.var_kelurahan', $filter)
					->group_end();
		}

		$order = 'mvk.var_nik ';
		switch($order_by){
			case 1 : $order = 'mvk.var_nik'; break;
			case 2 : $order = 'mvk.var_nama'; break;
			case 4 : $order = 'mk.var_kecamatan'; break;
			case 5 : $order = 'ml.var_kelurahan'; break;
		}
		
		if($limit > 0){
			$this->db->limit($limit, $ofset);
		}
		return $this->db->order_by($order, $sort)->get()->result();
	}
	
	public function listCount($kecamatan_filter = 0, $kelurahan_filter = 0, $status_filter = "", $filter = NULL){
		$this->db->select("*")
					->from($this->m_verifikasi_ktp." mvk")
					->join($this->t_verifikasi_brm." tvb", "mvk.var_nik = tvb.var_nik", "left")
					->join($this->m_kecamatan." mk", "mvk.int_kecamatan_id = mk.int_kecamatan_id", "left")
					->join($this->m_kelurahan." ml", "(mvk.`int_kecamatan_id` = ml.`int_kecamatan_id` AND mvk.`int_kelurahan_id` = ml.`int_kelurahan_id`)", "left")
					->where('mvk.int_kategori_ktp_id', 1);

		if(($kecamatan_filter != 0)){ // filter
			$this->db->where('mvk.int_kecamatan_id', $kecamatan_filter);
		}
			
		if(($kelurahan_filter != 0)){ // filter
			$this->db->where('mvk.int_kelurahan_id', $kelurahan_filter);
		}

		if(($status_filter == 1)){ // filter
			$this->db->where('tvb.int_verifikasi_brm_id != ""');
		}

		if(!empty($filter)){ // filters
            $filter = $this->filterAlphaNumeric($filter);
            $this->db->group_start()
			->like('mvk.var_nik', $filter)
			->or_like('mvk.var_nama', $filter)
			->or_like('mk.var_kecamatan', $filter)
			->or_like('ml.var_kelurahan', $filter)
			->group_end();
	    }
		return $this->db->count_all_results();
	}

	public function add($int_mverifikasi_ktp_id){
		$data =  $this->db->select("*")
						->from($this->m_verifikasi_ktp." mvk")
						->join($this->m_jeniskelamin." mj", "mvk.int_jeniskelamin_id = mj.int_jeniskelamin_id", "left")
						->join($this->m_status_kawin." msk", "mvk.int_status_kawin_id = msk.int_status_kawin_id", "left")
						->join($this->m_kecamatan." mkc", "mvk.int_kecamatan_id = mkc.int_kecamatan_id", "left")
						->join($this->m_kelurahan." mkl", "mvk.int_kelurahan_id = mkl.int_kelurahan_id", "left")
						->where('mvk.int_mverifikasi_ktp_id', $int_mverifikasi_ktp_id)
						->where('mvk.int_kategori_ktp_id', 1)->get()->row_array();

		return (object) array_merge($data);
	}
				

	public function create($ins, $lampiran = []){
		$var_jenis_img = $ins['var_jenis_img'];
		$ins['created_at'] = date("Y-m-d H:i:s");
		$ins['created_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$ins = $this->clearFormInsert($ins, ['lampiran', 'var_jenis_img', 'img_select', 'sidaku_token', 'cdImg']);

		$this->db->insert($this->t_verifikasi_brm, $ins);
		$int_verifikasi_brm_id = $this->db->insert_id();
		$this->setQueryLog('ins_posts');

		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_verifikasi_brm_id' => $int_verifikasi_brm_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_verifikasi_brm_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}

		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}	

	public function get($int_verifikasi_brm_id, $with_lampiran = true){
		$data =  $this->db->select("*")
						->from($this->t_verifikasi_brm." tvb")
						->join($this->m_verifikasi_ktp." mvk", "tvb.var_nik = mvk.var_nik", "left")
						->join($this->m_jeniskelamin." mj", "mvk.int_jeniskelamin_id = mj.int_jeniskelamin_id", "left")
						->join($this->m_status_kawin." msk", "mvk.int_status_kawin_id = msk.int_status_kawin_id", "left")
						->join($this->m_kecamatan." mkc", "mvk.int_kecamatan_id = mkc.int_kecamatan_id", "left")
						->join($this->m_kelurahan." mkl", "mvk.int_kelurahan_id = mkl.int_kelurahan_id", "left")
						->where('tvb.int_verifikasi_brm_id', $int_verifikasi_brm_id)->get()->row_array();

		if(!empty($data)){
			$lampiran = ($with_lampiran)? $this->get_lampiran($int_verifikasi_brm_id) : null;
		}

		return (object) array_merge($data, ['lampiran' => $lampiran]);
	}
				
	public function get_lampiran($int_verifikasi_brm_id){
		return $this->db->query("SELECT *
								FROM  {$this->t_verifikasi_brm_img} 
								WHERE int_verifikasi_brm_id = {$int_verifikasi_brm_id}")->result();
			
	}

	public function update($int_verifikasi_brm_id, $upd, $lampiran = []){
		$var_jenis_img = $upd['var_jenis_img'];
		$upd['updated_at'] = date("Y-m-d H:i:s");
		$upd['updated_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$upd = $this->clearFormInsert($upd, ['lampiran', 'var_jenis_img', 'img_select', 'sidaku_token', 'cdImg']);

		$this->db->where('int_verifikasi_brm_id', $int_verifikasi_brm_id);
		$this->db->update($this->t_verifikasi_brm, $upd);
		$this->setQueryLog('upd_brm');
		
		if(!empty($lampiran)){
			$lampiran_insert = [];
			foreach($lampiran as $lmp){
				$lampiran_insert[] = ['int_verifikasi_brm_id' => $int_verifikasi_brm_id,
									'var_image' => $this->db->escape($lmp['dir'].'/'.$lmp['file_name']),
									'var_jenis_img' => $this->db->escape($var_jenis_img)];
			}
			$this->db->insert_batch($this->t_verifikasi_brm_img, $lampiran_insert, false);
			$this->setQueryLog('ins_lampiran');
		}
				
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function delete($int_verifikasi_brm_id){
		$ins['deleted_at'] = date("Y-m-d H:i:s");
		$ins['deleted_by'] = $this->session->userdata['user_id'];
		$this->db->trans_begin();
		$this->db->delete($this->t_verifikasi_brm,  ['int_verifikasi_brm_id' => $int_verifikasi_brm_id]);
		if ($this->db->trans_status() === FALSE){
			$this->db->trans_rollback();
			return false;
		}else{
			$this->db->trans_commit();
			return true;
		}
	}

	public function get_lampiran_img($int_verifikasi_brm_img_id){
		return $this->db->query("SELECT *
								FROM 	{$this->t_verifikasi_brm_img} 
								WHERE 	int_verifikasi_brm_img_id = {$int_verifikasi_brm_img_id}")->row();
	}
	
	public function delete_lampiran_img($int_verifikasi_brm_img_id){
		$data = $this->get_lampiran_img($int_verifikasi_brm_img_id);
		if(!empty($data)){
			//$upload_path= $this->config->item('upload_path');
			//$img_dir = $this->config->item('img_dir');
			//$unlink_dir = str_replace($img_dir,"",$upload_path);
			unlink(FCPATH.'/'.$data->var_image);
			
			$this->db->trans_begin();
			$this->db->query("DELETE FROM {$this->t_verifikasi_brm_img} WHERE int_verifikasi_brm_img_id = {$int_verifikasi_brm_img_id}");
			if ($this->db->trans_status() === FALSE){
				$this->db->trans_rollback();
				return false;
			}else{
				$this->db->trans_commit();
				return true;
			}
		}else{
			return false;
		}
	}
}
