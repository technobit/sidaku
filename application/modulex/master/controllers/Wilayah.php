<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends MX_Controller {

	function __construct(){
        parent::__construct();
		
		$this->kodeMenu = 'MASTER-WILAYAH';
        $this->module   = 'sistem';
        $this->routeURL = 'master_wilayah';

		$this->load->model('wilayah_model', 'model');
        $this->config->set_item('compress_output', FALSE);
    }
	
    public function index(){
		return null;
	}
	public function get_kecamatan(){
		$data = $this->model->get_kecamatan($this->input->post('id', true));
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]));
        }

	}
	public function get_kelurahan(){
		$data = $this->model->get_kelurahan($this->input->post('int_kecamatan_id', true));
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]));
        }

	}

	public function get_rw(){
        $data = $this->model->get_rw($this->input->post('int_kecamatan_id', true),$this->input->post('int_kelurahan_id', true));
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]), 200, false);
        }
    }
	public function get_rt(){
        $data = $this->model->get_rt($this->input->post('int_kecamatan_id', true), $this->input->post('int_kelurahan_id', true), $this->input->post('var_rw', true));
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]), 200, false);
        }
    }
	public function get_kelurahan_usr(){
		$data = $this->model->get_kelurahan_usr($this->input->post('int_kecamatan_id', true));
        if(empty($data)){
		    $this->set_json(array( 'stat' => false, 
								'data' => NULL,
								'csrf' => [ 'name' => $this->getCsrfName(),
                                'token' => $this->getCsrfToken()]));
        }else{
            $this->set_json(array( 'stat' => TRUE, 
                                    'data' => $data,
                                    'csrf' => [ 'name' => $this->getCsrfName(),
                                    'token' => $this->getCsrfToken()]));
        }

	}

}
