<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['imp_verifikasi']['get']                         = 'master/imp_verifikasi';
$route['imp_verifikasi']['post']                        = 'master/imp_verifikasi/list';
$route['imp_verifikasi/add']['get']                     = 'master/imp_verifikasi/add';
$route['imp_verifikasi/save']['post']                   = 'master/imp_verifikasi/save';
$route['imp_verifikasi/([a-zA-Z0-9]+)']['get']     	    = 'master/imp_verifikasi/get/$1';
$route['imp_verifikasi/([a-zA-Z0-9]+)']['post']    	    = 'master/imp_verifikasi/update/$1';
$route['imp_verifikasi/([a-zA-Z0-9]+)/del']['get'] 	    = 'master/imp_verifikasi/confirm/$1';
$route['imp_verifikasi/([a-zA-Z0-9]+)/del']['post']	    = 'master/imp_verifikasi/delete/$1';

$route['nik/cek/([0-9]+)']['post']  = 'master/nik/cek/$1';

$route['wilayah/kelurahan']['post'] = 'master/wilayah/get_kelurahan';
$route['wilayah/rw']['post']        = 'master/wilayah/get_rw';
$route['wilayah/rt']['post']        = 'master/wilayah/get_rt';

$route['wilayah/kelurahan_usr']['post'] = 'master/wilayah/get_kelurahan_usr';
