<?php
(defined('BASEPATH')) OR exit('No direct script access allowed');

class Wilayah_model extends MY_Model {

	public function get_kecamatan(){
		return $this->db->query("SELECT *
				FROM	{$this->m_kecamatan}
				ORDER BY int_kecamatan_id")->result();
	}

	public function get_kelurahan($int_kecamatan_id){
		if ($int_kecamatan_id === ""){
			return false;
		}else{
			return $this->db->query("SELECT *
					FROM {$this->m_kelurahan}
					WHERE int_kecamatan_id = {$int_kecamatan_id}
					ORDER BY int_kelurahan_id")->result();
		}
	}

	public function get_rw($int_kecamatan_id = "", $int_kelurahan_id = ""){
		if ($int_kecamatan_id === "" || $int_kelurahan_id === ""){
			return false;
		}else{
			return $this->db->query("SELECT *
					FROM {$this->m_rw}
					WHERE int_kecamatan_id = {$int_kecamatan_id} 
						AND  int_kelurahan_id = {$int_kelurahan_id}
					ORDER BY var_rw")->result();
		}
	}

	public function get_rt($int_kecamatan_id = "", $int_kelurahan_id = "", $var_rw = ""){
		if ($int_kecamatan_id === "" || $int_kelurahan_id === "" || $var_rw === ""){
			return false;
		}else{
			return $this->db->query("SELECT *
					FROM {$this->m_rt}
					WHERE int_kecamatan_id = {$int_kecamatan_id} 
						AND  int_kelurahan_id = {$int_kelurahan_id} 
						AND var_rw = '{$var_rw}'
					ORDER BY var_rt")->result();
		}
	}

	public function get_kecamatan_usr(){
		$this->db->select("*")
					->from($this->m_kecamatan);

		if( $this->session->userdata['int_kecamatan_id'] != 0){ // filters
            $this->db->where('int_kecamatan_id', $this->session->userdata['int_kecamatan_id']);
		}

		return $this->db->order_by('int_kecamatan_id', 'ASC')->get()->result();

	}

	public function get_kelurahan_usr($int_kecamatan_id){
		if ($int_kecamatan_id === ""){
			return false;
		}else{
			$this->db->select("*")
						->from($this->m_kelurahan)
						->where('int_kecamatan_id', $int_kecamatan_id);

			if( $this->session->userdata['int_kelurahan_id'] != "" && $this->session->userdata['int_kelurahan_id'] != 0){ // filters
				$this->db->where('int_kelurahan_id', $this->session->userdata['int_kelurahan_id']);
			}

			return $this->db->order_by('int_kecamatan_id', 'ASC')->get()->result();

		}
	}
}
